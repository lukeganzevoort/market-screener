import http.client
import json
import data_manager
import strategies
import numpy as np
import itertools
import datetime
import pandas_market_calendars as mcal
import market_time
import asciichartpy
from beautifultable import BeautifulTable
from collections import namedtuple
import matplotlib
import matplotlib.pyplot as plt

# Design Implementation
# Factors of determining the best options
# - Potential Gain
# - Probability of that gain happening
#
# The Probability should be based on the following factors
# - Historical Data for 5 years, 1 year, 6 months, 3 months, 1 month, 2 weeks, 1 week
# - Volitility
# - Implied Volitility
# - Historical Options prices (this might be the same as implied volitility)

def get_option_symbol(symbol, date, option_type, strike):
    pass

def get_stock_data(symbol):
    # Request: Market Quotes (https://sandbox.tradier.com/v1/markets/quotes?symbols=spy)
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    # Headers
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/quotes?symbols='+symbol), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # print('Response status ' + str(response.status))
        return (content['quotes']['quote'])
    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_options_chain(symbol, expiration):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/options/chains?symbol='+symbol+'&expiration='+expiration), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['options']['option'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_options_expirations(symbol):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/options/expirations?symbol='+symbol), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['expirations']['date'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_calendar():
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', '/v1/markets/calendar', None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['calendar']['days'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_daily_historical_data(symbol, start_date_str):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/history?symbol='+symbol+'&interval=daily&start='+start_date_str), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['history']['day'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

# def get_option_chain_and_junk(symbol):
#     # Request: Market Quotes (https://sandbox.tradier.com/v1/markets/quotes?symbols=spy)
#     connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
#     # Headers
#     headers = {"Accept":"application/json",
#                "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
#     connection.request('GET', ('/v1/markets/options/chains?symbol='+symbol+'&expiration=2019-01-11'), None, headers)
#     calls = {}
#     try:
#         response = connection.getresponse()
#         content = json.loads(response.read())
#         # Success
#         print('Response status ' + str(response.status))
#         opt_list = sorted(content["options"]["option"], key=lambda k:(k["root_symbol"], k["option_type"], k["strike"]))
#         for opt in opt_list:
#             # print(opt["symbol"], opt["root_symbol"], opt["strike"], opt["option_type"], opt["ask"])
#             # call_strike = opt['strike']
#             # stock_cost = get_stock_live_data(symbol)['ask']
#             # call_credit = opt['bid']
#             # stock_close_price = stock_cost * 1.1
#             # a = strategies.covered_call_profit (call_strike, stock_cost, call_credit, stock_close_price)
#             # print(call_strike, stock_cost, call_credit, stock_close_price, a)
#             if opt['strike'] == 257 and opt['option_type'] == 'call':
#                 print("hello!", opt['bid'])
#                 aa = []
#                 stock = get_stock_live_data(symbol)
#                 print(stock)
#                 bottom = int(stock['ask']*.9)
#                 top = int(stock['ask']*1.1)+1
#                 for num in range(bottom, top):
#                     a = strategies.covered_call_profit (opt['strike'], stock['ask'], opt['bid'], num)
#                     percent_change = ((num/stock['ask']) - 1) * 100
#                     b = data_manager.probability_stock_close_above_threshold_in_week('market-data.hdf5', symbol, percent_change)
#                     print(num, a, percent_change, b)
#                     aa.append([num,a['percent']*100,b,-1, -1])
#                 print('strike','percent gain','probability')
#
#                 for idx in range(len(aa)-1):
#                     aa[idx][3] = aa[idx][2] - aa[idx+1][2]
#                     aa[idx][4] = (aa[idx][1] + aa[idx+1][1]) / 2
#                     print(aa[idx])
#                 aa[len(aa)-1][3] = aa[len(aa)-1][2]
#                 aa[len(aa)-1][4] = aa[len(aa)-1][1]
#                 print("hi", aa[len(aa)-1])
#                 print(sum(np.array(aa)[:,3]))
#                 bb = []
#                 for el in aa:
#                     bb.append(el[4]*el[3])
#                 print(bb)
#                 print(sum(bb))
#
#     except http.client.HTTPException:
#         # Exception
#         print('Exception during request')

def probability_array_for_year_for_x_days2(symbol, days):
    now_dt = datetime.datetime.now()
    today_dt = datetime.datetime(now_dt.year, now_dt.month, now_dt.day)
    today_epoch = market_time.datetime_to_epoch(today_dt)
    year_back_dt = datetime.datetime(today_dt.year - 1, today_dt.month, today_dt.day)
    year_back_epoch = market_time.datetime_to_epoch(year_back_dt)
    year_back_str = year_back_dt.strftime('%Y-%m-%d')

    data = get_daily_historical_data(symbol, year_back_str)

    for el in data:
        if el['volume'] <= 0:
            raise 'Zero Volume... do something to filter these'

    # data = data_manager.read_historicals_from_hdf5_2d('market-data.hdf5', symbol, year_back_dt.year, 'daily')
    # data = np.append(data, data_manager.read_historicals_from_hdf5_2d('market-data.hdf5', symbol, today_dt.year, 'daily'), 0)
    # data = np.array(data)[np.logical_not(np.logical_or(data[:,5] == 0, data[:,0] < year_back_epoch, data[:,0] >= today_epoch))]

    percent_changes = []
    days_ahead = days - 1
    for idx in range(len(data)):
        if (idx + days_ahead) < len(data):
            open = data[idx]['open']
            close = data[idx+days_ahead]['close']
            percent_changes.append(((close / open) - 1) * 100)

    return percent_changes

def probability_array_for_year_for_x_days(symbol, days):
    now_dt = datetime.datetime.now()
    today_dt = datetime.datetime(now_dt.year, now_dt.month, now_dt.day)
    today_epoch = market_time.datetime_to_epoch(today_dt)
    year_back_dt = datetime.datetime(today_dt.year - 1, today_dt.month, today_dt.day)
    year_back_epoch = market_time.datetime_to_epoch(year_back_dt)

    data = data_manager.read_historicals_from_hdf5_2d('market-data.hdf5', symbol, year_back_dt.year, 'daily')
    data = np.append(data, data_manager.read_historicals_from_hdf5_2d('market-data.hdf5', symbol, today_dt.year, 'daily'), 0)
    data = np.array(data)[np.logical_not(np.logical_or(data[:,5] == 0, data[:,0] < year_back_epoch, data[:,0] >= today_epoch))]

    x = []
    days_ahead = days - 1
    for idx in range(len(data)):
        if (idx + days_ahead) < len(data):
            open = data[idx][1]
            close = data[idx][2]
            x.append(((close / open) - 1) * 100)

    return x

def get_probability_for_exp(symbol, exp_str):
    today_dt = datetime.datetime.now()
    today_str = today_dt.strftime('%Y-%m-%d')
    # exp_dt = datetime.datetime.strptime(exp_str, '%Y-%m-%d')
    days_to_exp = len(mcal.get_calendar('NYSE').valid_days(start_date=today_str, end_date=exp_str))
    # print(exp_str, days_to_exp)
    percent_change_list = probability_array_for_year_for_x_days(symbol, days_to_exp)
    return(percent_change_list)

def buy_call(expiration, strike, option_cost, stock_close_price):
    profit = -option_cost
    if stock_close_price > strike:
        profit = profit + (stock_close_price - strike)
    return profit

def sell_call(expiration, strike, option_cost, stock_close_price):
    profit = option_cost
    if stock_close_price > strike:
        profit = profit - (stock_close_price - strike)
    return profit

def buy_put(expiration, strike, option_cost, stock_close_price):
    profit = -option_cost
    if stock_close_price < strike:
        profit = profit + (strike - stock_close_price)
    return profit

def sell_put(expiration, strike, option_cost, stock_close_price):
    profit = option_cost
    if stock_close_price < strike:
        profit = profit - (strike - stock_close_price)
    return profit

def buy_stock(stock_price, stock_close_price):
    profit = stock_close_price - stock_price
    return profit

# Buy a call +1
# Sell a call -1
# Sell a put -1



    # begin = datetime.datetime(2014,1,1)
    # begin = datetime.datetime(2018,1,2)
    # end = datetime.datetime(2018,12,31)
    # begin_sec = market_time.datetime_to_epoch(begin)
    # end_sec = market_time.datetime_to_epoch(end)
    # range_of_years = range(begin.year, end.year + 1)
    # interval = "daily"
    # per_chg_list = []
    #
    # for year in range_of_years:
    #     dat = read_historicals_from_hdf5_2d(file_name, symbol, year, interval)
    #     for row in dat:
    #         if (row[1] != 0 and row[2] != 0 and row[0] > begin_sec and row[0] < end_sec):
    #             perc_chg = ((row[2] / row[1]) - 1) * 100
    #             per_chg_list.append(perc_chg)
    #
    # prob_above_threshold = (sum((percent_changed > threshold_percent) for percent_changed in per_chg_list) / len(per_chg_list))
    # return prob_above_threshold

def print_percent(number):
    print(round(number, 2), '% Gain')

def annualize_percentage(percent_gain, period_days):
    annual_market_days = 252
    possible_annual_occurances = annual_market_days / period_days
    annualized_perc_gain = (((((percent_gain / 100) + 1)**possible_annual_occurances) - 1) * 100)
    return annualized_perc_gain

def plot_array_in_term(array):
    counts, bins = np.histogram(array, bins=40, range=(-5,5))
    print(bins[19], 'to', bins[20], counts[19])
    print(bins[20], 'to', bins[21], counts[20])
    print(asciichartpy.plot(counts, {'height':10}))

def run_it():
    symbols = ['BAC']
    strats = ['covered_call']
    experations = ['2019-01-11']
    strikes = [260]

    for symbol in symbols:
        print('---', symbol, '---')
        # Get live symbol data
        startTime = datetime.datetime.now()
        stock_data = get_stock_data(symbol)
        print('Get', symbol, 'Runtime:', (datetime.datetime.now() - startTime))
        # Get option expirations
        startTime = datetime.datetime.now()
        opt_expirations = get_options_expirations(symbol)
        print('Get Option expirations Runtime:', (datetime.datetime.now() - startTime))
        # Get all option chains for this symbol
        startTime = datetime.datetime.now()
        opt_list = [sorted(get_options_chain(symbol, exp), key=lambda
            k:(k['strike'], k['option_type'])) for exp in opt_expirations]
        print('Get Option Chains Runtime:', (datetime.datetime.now() - startTime))
        # Get Probability of stock close
        startTime = datetime.datetime.now()
        percent_change_lists = [get_probability_for_exp2(symbol, exp) for exp in opt_expirations]
        print('Get Probability Runtime:', (datetime.datetime.now() - startTime))
        # print(percent_change_lists)
        # plot_array_in_term(percent_change_lists[0])
        # Test strategies
        startTime = datetime.datetime.now()
        test_strategies(stock_data, opt_expirations, opt_list, percent_change_lists)
        print('Test Strategies Runtime:', (datetime.datetime.now() - startTime))

def test_covered_call_with_chain(stock_data, opt_chain, percent_change_list):
    opt_gain_prob = []
    for opt in opt_chain:
        if opt['option_type'] == 'call' and opt['bid'] > 0:
            gain_possibilities = []
            for perc_chg in percent_change_list:
                close = percent_change_to_total_decimal(perc_chg) * stock_data['ask']
                gain_possibilities.append(strategies.covered_call_profit(opt['strike'], stock_data['ask'], opt['bid'], close))
            opt_gain_prob.append(gain_possibilities['percent'])

def get_avg_percent_gain(strategy_method, strike, stock_ask, opt_bid, percent_change_list):
    possible_profits = []
    for perc_chg in percent_change_list:
        close = percent_change_to_total_decimal(perc_chg) * stock_ask
        profit_details = strategy_method(strike, stock_ask, opt_bid, close)
        possible_profits.append(profit_details)
    avg = np.mean(per for per in possible_profits['percent'])
    return avg


def percent_change_to_total_decimal(percent_change):
    return ((percent_change / 100) + 1)

def test_strategies(stock_data, opt_exps, opt_chains, percent_change_lists):
    lst = []
    details = []
    for idx, chain in enumerate(opt_chains):
        # test_covered_call_with_chain(stock_data, chain, percent_change_lists[idx])
        for opt in chain:
            if opt['bid'] == None or opt['bidsize'] == None:
                print(opt['description'], opt['bidsize'], opt['bid'])

            if opt['option_type'] == 'call' and opt['bid'] > 0:
                opt_gain_possibilities = []
                for perc_chg in percent_change_lists[idx]:
                    close = (perc_chg / 100 + 1) * stock_data['ask']
                    # print('Strike:', opt['strike'], 'Bid:', opt['bid'], 'Stock Close:', close, 'Percent Change:', perc_chg)
                    opt_gain_possibilities.append(strategies.covered_call_profit(opt['strike'], stock_data['ask'], opt['bid'], close)['percent'])
                today_str = datetime.datetime.now().strftime('%Y-%m-%d')
                days_to_exp = len(mcal.get_calendar('NYSE').valid_days(start_date=today_str, end_date=opt_exps[idx]))
                opt_avg_per_gain_annual = annualize_percentage(np.mean(opt_gain_possibilities), days_to_exp)
                lst.append(opt_avg_per_gain_annual)
                details.append([opt,opt_avg_per_gain_annual])
    # for percent_change in percent_change_lists[0]:
    #     close = (percent_change / 100 + 1) * stock_data['ask']
    #     p = strategies.covered_call_profit(opt_chains[exp][str]['strike'], stock_data['ask'], opt_chains[exp][str]['bid'], close)
    #     lst.append(p['percent'])
    # for x in itertools.product([1, 5, 8], [0.5, 4], [6,7]):
    #     print(x)
    # for x in itertools.product()
    for el in sorted(details, key=lambda k:(k[1]), reverse=True)[0:3]:
        print(el[0]['description'], '| Bid', el[0]['bidsize'], '@ $', el[0]['bid'], '| Gain', round(el[1],1), '% | Stock Ask Price $', stock_data['ask'])
        # print_percent(el[1])
        # print(el[0])
# dict_keys(['symbol', 'description', 'exch', 'type', 'last', 'change', 'change_percentage', 'volume', 'average_volume', 'last_volume', 'trade_date', 'open', 'high', 'low', 'close', 'prevclose', 'week_52_high', 'week_52_low', 'bid', 'bidsize', 'bidexch', 'bid_date', 'ask', 'asksize', 'askexch', 'ask_date', 'open_interest', 'underlying', 'strike', 'contract_size', 'expiration_date', 'expiration_type', 'option_type', 'root_symbol'])

# Dependants
#  Experation: 2019-01-11
#  Call/Put: call
#  Bid
#  Ask
#  Strike: 260
#  Strategy: covered call
#  Stock: SPY

def get_probability_for_exp2(symbol, exp_str):
    a = datetime.datetime.strptime(exp_str, '%Y-%m-%d').date()

    try:
        days_to_exp = (np.where(market_calendar.date == a)[0][0]) + 1
    except:
        raise('Please use a valid market day as the experation date.')

    percent_change_list = probability_array_for_year_for_x_days2(symbol, days_to_exp)

    return(percent_change_list)

class StockAndOption(object):

    def __init__(self, symbol):
        self.symbol = symbol
        self.strategy_methods = []
        self.results_tuples = []

    def update_live_stock(self):
        self.live_stock = get_stock_data(self.symbol)
        print('Stock', self.symbol, 'is trading at', self.live_stock['ask'])

    def get_live_stock(self):
        return self.live_stock

    def update_option_chains(self):
        # Get option expirations
        self.option_expirations = list(itertools.filterfalse(lambda x: x > (datetime.datetime.now() + datetime.timedelta(days=30)).strftime('%Y-%m-%d'), get_options_expirations(self.symbol)))
        # Get all option chains for this symbol
        self.option_chains = [sorted(get_options_chain(self.symbol, exp), key=lambda k:(k['strike'], k['option_type'])) for exp in self.option_expirations]

    def get_option_expirations(self):
        return self.option_expirations

    def get_option_chains(self):
        return self.option_chains

    def update_stock_historicals(self):
        now = datetime.datetime.now()
        year_back_str = datetime.datetime((now.year - 1), now.month, now.day).strftime('%Y-%m-%d')
        self.stock_historicals = get_daily_historical_data(self.symbol, year_back_str)
        for historical in self.stock_historicals:
            if historical['volume'] == 0:
                raise 'Zero Volume: Do something to filter these out'

    def get_stock_historicals(self):
        return self.stock_historicals

    def update_days_to_expirations(self):
        self.days_to_expirations = {}
        expirations_str = self.get_option_expirations()
        for exp_str in expirations_str:
            exp_as_date = datetime.datetime.strptime(exp_str, '%Y-%m-%d').date()
            try:
                # includes today as a full day
                self.days_to_expirations[exp_str] = ((np.where(market_calendar.date == exp_as_date)[0][0]) + 1)
            except:
                raise('Please use a valid market day as the experation date.')
        for exp in self.days_to_expirations:
            assert(self.days_to_expirations[exp] > 0)

    def get_days_to_expirations(self):
        return self.days_to_expirations

    def update_probability_plots(self):
        days_to_expirations = self.get_days_to_expirations()
        stock_historicals = self.get_stock_historicals()
        number_of_historicals = len(stock_historicals)
        prob_plots = {}
        mult_plots = {}
        for exp in days_to_expirations:
            prob_plot_for_exp = []
            mult_plot_for_exp = []
            for idx in range(number_of_historicals):
                if (idx + days_to_expirations[exp]) <= number_of_historicals:
                    open = stock_historicals[idx]['open']
                    close = stock_historicals[idx + days_to_expirations[exp] - 1]['close']
                    prob_plot_for_exp.append(((close - open) / open) * 100)
                    mult_plot_for_exp.append((close / open))
            prob_plots[exp] = prob_plot_for_exp
            mult_plots[exp] = mult_plot_for_exp
        self.probability_plots = prob_plots
        self.probability_multipliers = mult_plots

    def get_probability_plots(self):
        return self.probability_plots

    def set_strategy_method(self, strategy_profit_method):
        self.strategy_methods.append(strategy_profit_method)

    def get_strategy_methods(self):
        return self.strategy_methods

    def get_average_percent_gains(self, strategy, live_stock, option_1, option_2, option_3, option_4):
        if option_1['option_type'] == 'call' and option_2 == None and option_3 == None and option_4 == None and option_1['bid'] > 0:
            call_strike = option_1['strike']
            option_1_exp = option_1['expiration_date']
            stock_cost = live_stock['ask']
            call_credit = option_1['bid']
            profit = []
            days_to_exp = self.get_days_to_expirations()[option_1_exp]

            prob_plot = np.array(self.get_probability_plots()[option_1_exp])
            stock_close_price = stock_cost * ((prob_plot / 100) + 1)
            profit_return = strategies.covered_call_profit(call_strike, stock_cost, call_credit, stock_close_price)
            average_percent_gain = np.mean(profit_return['percent'])
            average_profit = np.mean(profit_return['profit'])
            initial_cost = profit_return['initial_cost']
            annual_percent_gain = annualize_percentage(average_percent_gain, days_to_exp)

            return annual_percent_gain

        return None

    def try_cc(self):
        avg_perc_gains = []
        summaries =[]
        strats = self.get_strategy_methods()
        chains = self.get_option_chains()
        stock = self.get_live_stock()
        for opt_chain in chains:
            for opt in opt_chain:
                if opt['option_type'] == 'call':
                    strat = Strat(stock)
                    strat.add_option(opt, 'sell')
                    if strat.test_results(self.probability_multipliers[opt['expiration_date']]):
                        summaries.append(strat.get_detailed_tuple_summary())
                temp = (self.get_average_percent_gains('CC', stock, opt, None, None, None))
                if temp != None:
                    avg_perc_gains.append({'percent':temp,'option':opt['description'],'opt_bid':opt['bid'],'stock_ask':stock['ask']})
        self.results_tuples = sorted(avg_perc_gains, key=lambda k:k['percent'], reverse = True)
        self.results_tuples2 = sorted(summaries, key=lambda k:k.results.annualized_multiplier, reverse = True)
        for o in self.results_tuples2[:10]:
            print(o.results.annualized_multiplier)
    #
    # def try_ic2(self):
    #     summaries =[]
    #     chains = self.get_option_chains()
    #     stock = self.get_live_stock()
    #     startTime = datetime.datetime.now()
    #     for opt_chain in chains[3:4]:
    #         for opt1 in opt_chain[50:54]:
    #             for opt2 in opt_chain[50:54]:
    #                 for opt3 in opt_chain:
    #                     for opt4 in opt_chain:
    #                         if (opt1['option_type'] == 'put' and opt2['option_type'] == 'put'
    #                             and opt3['option_type'] == 'call' and opt4['option_type'] == 'call'
    #                             and opt1['strike'] <= opt2['strike'] and opt2['strike'] <= opt3['strike']
    #                             and opt3['strike'] <= opt4['strike'] and opt1['ask'] > 0 and opt2['bid'] > 0
    #                             and opt3['bid'] > 0 and opt4['ask'] > 0):
    #                             strat = Strat(stock)
    #                             strat.add_option(opt1, 'buy')
    #                             strat.add_option(opt2, 'sell')
    #                             strat.add_option(opt3, 'sell')
    #                             strat.add_option(opt4, 'buy')
    #
    #                             if strat.test_results(self.probability_multipliers[opt1['expiration_date']]):
    #                                 summaries.append(strat.get_detailed_tuple_summary())
    #     print("IC2 Runtime: ", (datetime.datetime.now() - startTime), '  Summary Length: ', len(summaries))
    #     self.results_tuples = sorted(summaries, key=lambda k:k.results.annualized_multiplier, reverse = True)
    #     for o in self.results_tuples[:3]:
    #         print(o.results.annualized_multiplier, o.options[0].strike, o.options[1].strike, o.options[2].strike, o.options[3].strike)
    #
    #
    # def try_ic3(self):
    #     summaries = []
    #     chains = self.get_option_chains()
    #     stock = self.get_live_stock()
    #     startTime = datetime.datetime.now()
    #     sell_call_func = lambda x: (x['option_type'] != 'call' or x['bid'] == 0)
    #     buy_call_func = lambda x: (x['option_type'] != 'call' or x['ask'] == 0)
    #     sell_put_func = lambda x: (x['option_type'] != 'put' or x['bid'] == 0)
    #     buy_put_func = lambda x: (x['option_type'] != 'put' or x['ask'] == 0)
    #
    #     for opt_chain in chains[3:4]:
    #         a = itertools.product( itertools.filterfalse( buy_put_func, opt_chain[50:70]),
    #             itertools.filterfalse( sell_put_func, opt_chain[50:70]),
    #             itertools.filterfalse( sell_call_func, opt_chain[50:70]),
    #             itertools.filterfalse( buy_call_func, opt_chain[50:70]))
    #         while True:
    #             opt_comb = next(a, None)
    #             if opt_comb == None:
    #                 break
    #             if opt_comb[0]['strike'] <= opt_comb[1]['strike'] and opt_comb[1]['strike'] <= opt_comb[2]['strike'] and opt_comb[2]['strike'] <= opt_comb[3]['strike']:
    #                 strat = Strat(stock)
    #                 strat.add_option(opt_comb[0], 'buy')
    #                 strat.add_option(opt_comb[1], 'sell')
    #                 strat.add_option(opt_comb[2], 'sell')
    #                 strat.add_option(opt_comb[3], 'buy')
    #                 if strat.test_results(self.probability_multipliers[opt_comb[0]['expiration_date']]):
    #                     summaries.append(strat.get_detailed_tuple_summary())
    #                     # summaries = summaries + 1
    #                 # print(opt_comb[0]['description'], opt_comb[1]['description'], opt_comb[2]['description'], opt_comb[3]['description'])
    #
    #     print("IC3 Runtime: ", (datetime.datetime.now() - startTime), '  Summary Length: ', len(summaries))
    #     self.results_tuples = sorted(summaries, key=lambda k:k.results.annualized_multiplier, reverse = True)
    #     for o in self.results_tuples[:3]:
    #         print(o.results.annualized_multiplier)
    #
    # def try_ic4(self):
    #     summaries = []
    #     chains = self.get_option_chains()
    #     stock = self.get_live_stock()
    #     startTime = datetime.datetime.now()
    #     sell_call_func = lambda x: (x['option_type'] != 'call' or x['bid'] == 0)
    #     buy_call_func = lambda x: (x['option_type'] != 'call' or x['ask'] == 0)
    #     sell_put_func = lambda x: (x['option_type'] != 'put' or x['bid'] == 0)
    #     buy_put_func = lambda x: (x['option_type'] != 'put' or x['ask'] == 0)
    #     ordered_func = lambda x: (x[0]['strike'] > x[1]['strike'] or x[1]['strike'] > x[2]['strike'] or x[2]['strike'] > x[3]['strike'])
    #     for opt_chain in chains[3:4]:
    #         a = itertools.filterfalse( ordered_func,
    #             itertools.product(
    #             itertools.filterfalse( buy_put_func, opt_chain[50:70]),
    #             itertools.filterfalse( sell_put_func, opt_chain[50:70]),
    #             itertools.filterfalse( sell_call_func, opt_chain[50:70]),
    #             itertools.filterfalse( buy_call_func, opt_chain[50:70])))
    #         while True:
    #             opt_comb = next(a, None)
    #             if opt_comb == None:
    #                 break
    #             # assert( opt_chain[opt_comb[0]]['strike'] <= opt_chain[opt_comb[1]]['strike'] and opt_chain[opt_comb[1]]['strike'] <= opt_chain[opt_comb[2]]['strike'] and opt_chain[opt_comb[2]]['strike'] <= opt_chain[opt_comb[3]]['strike'])
    #             # summaries = summaries + 1
    #             # print(opt_comb, opt_chain[opt_comb[0]]['symbol'])
    #             strat = Strat(stock)
    #             strat.add_option(opt_comb[0], 'buy')
    #             strat.add_option(opt_comb[1], 'sell')
    #             strat.add_option(opt_comb[2], 'sell')
    #             strat.add_option(opt_comb[3], 'buy')
    #             if strat.test_results(self.probability_multipliers[opt_comb[0]['expiration_date']]):
    #                 summaries.append(strat.get_detailed_tuple_summary())
    #                 # summaries = summaries + 1
    #             else:
    #                 raise ValueError('strat not valid')
    #             # print(opt_comb[0]['description'], opt_comb[1]['description'], opt_comb[2]['description'], opt_comb[3]['description'])
    #
    #     print("IC4 Runtime: ", (datetime.datetime.now() - startTime), '  Summary Length: ', len(summaries))
    #     self.results_tuples = sorted(summaries, key=lambda k:k.results.annualized_multiplier, reverse = True)
    #     for o in self.results_tuples[:3]:
    #         print(o.results.annualized_multiplier)

    def try_ic5(self, expiration_date):
        # DONE: Adjust the results to be categorized by strategy
        # DONE: Adjust the results to show 12M, 6M, 3M, 4W, 2W, 1W back testing
        # DONE: Figure out why some of the strategies are 100% successful when clearly that can't be the case. Like buying an expensive call and selling a cheap put.
        # TODO: Fix buying and selling the same option (that's dumb)
        #   - I want to be able to pass something that says option type and quantity and then doesn't grab duplicate options
        # TODO: Add functionality to tell it to purchase stock
        # TODO: Add functionality to tell it to hold cash to purchase stock
        # TODO: shorten calculation time
        # TODO: Add functionality for 6 option strategies
        # strategy_types =
        strategy_types = (('Buy Call', 'Buy Call', 'Sell Call', 'Sell Call')#, '', '')
                # *Long Butterfly Spread w/Calls (buy,sell2,buy)*
                # *Skip Strike Butterfly w/Calls (buy,sell2,skip,buy)*
                # *Inverse Skip Strike Butterfly w/Calls (sell,buy2,skip,sell)*
                # *Long Condor Spread W/Calls (buy,sell,sell,buy)*
            ,('Buy Call', 'Buy Call', 'Sell Call', None)#, '', '')
                # *Back Spread w/ Calls (sell,buy 2x)*
            ,('Buy Call', 'Sell Call', 'Buy Put', 'Sell Put')#, '', '')
                # *Iron Butterfly (buy put,sell put and call,buy call)*
                # *Iron Condor (buy put, sell put, sell call, buy call)*
            ,('Buy Call', 'Sell Call', None, None)#, '', '')
                # *Long Call Spread (buy,sell)*
                # *Short Call Spread (sell,buy)*
            ,('Buy Call', 'Buy Put', None, None)#, '', '')
                # *Long Straddle (same strike)*
                # *Long Strangle (put,call)*
            ,('Buy Call', 'Sell Put', None, None)#, '', 'Cash')
                # *Long Combination (same strike)*
            ,('Buy Call', None, None, None)#, '', '')
                # *Long Call*
            # ,('Sell Call', 'Buy Put', None, None, 'Purchase', '')
                # *Collar*
            # ,('Sell Call', None, None, None, 'Purchase', '')
                # *Covered Call*
            ,('Buy Put', 'Buy Put', 'Sell Put', 'Sell Put')#, '', '')
                # *Long Butterfly Spread w/Puts (buy,sell2,buy)*
                # *Skip Strike Butterfly w/Puts (buy,skip,sell2,buy)*
                # *Inverse Skip strike Butterfly w/Puts (sell,skip,buy2,sell)*
                # *Long Condor Spread w/Puts (buy,sell,sell,buy)*
            ,('Buy Put', 'Buy Put', 'Sell Put', None)#, '', '')
                # *Back Spread w/ Puts (buy 2x,sell)*
            ,('Buy Put', 'Sell Put', None, None)#, '', '')
                # *Long Put Spread (sell,buy)*
                # *Short Put Spread (buy,sell)*
            ,('Buy Put', None, None, None)#, '', '')
                # *Long Put*
            # ,('Buy Put', None, None, None, 'Purchase', '')
                # *Protective Put*
            ,('Sell Put', None, None, None)#, '', 'Cash')
                # *Cash-Secured Put*
            #,('Buy Call', 'Buy Call', 'Buy Call', 'Sell Call', 'Sell Call', 'Sell Call')
                # *Christmas Tree Butterfly w/Calls (buy,skip,sell3,buy2)*
            #,('Buy Put', 'Buy Put', 'Buy Put', 'Sell Put', 'Sell Put', 'Sell Put')
                # *Christmas Tree Butterfly w/Calls (buy2,sell3,skip,buy)*
        )
        summaries = []
        blacklist_strikes = []
        cnt3 = 0
        cnt = 0
        opt1 = ''
        chains = self.get_option_chains()
        stock = self.get_live_stock()
        stock_ask = stock['ask']
        startTime = datetime.datetime.now()
        opt_func = lambda x: (x['bid'] == 0 or x['ask'] == 0) if x else False
        buy_call_func = lambda x: (x['option_type'] != 'call' or x['ask'] == 0)
        sell_put_func = lambda x: (x['option_type'] != 'put' or x['bid'] == 0)
        buy_put_func = lambda x: (x['option_type'] != 'put' or x['ask'] == 0)
        ordered_func = lambda x: (x[0]['strike'] > x[1]['strike'] or x[1]['strike'] > x[2]['strike'] or x[2]['strike'] > x[3]['strike'])
        double_sell_call_func = lambda x: (sum([qnt for opt, qnt in zip(x[0], x[1]) if opt if opt['option_type'] == 'call']) < 0)
        double_sell_put_func = lambda x: (sum([qnt for opt, qnt in zip(x[0], x[1]) if opt if opt['option_type'] == 'put'] ) < 0)
        double_none_func = lambda x: [True for opt, qnt in zip(x[0], x[1]) if (not opt and qnt < 0)]
        opt_chain = None
        for chain in chains:
            if chain[0]['expiration_date'] == expiration_date:
                opt_chain = chain
        if opt_chain == None:
            raise ValueError('No chain matches that expiration date')
        # TODO: add support to buy multiple of the same option
        stock_close_prices = np.round_(np.array(self.probability_multipliers[opt_chain[0]['expiration_date']]) * stock_ask, decimals=12)
        calls_chain = []
        puts_chain = []
        for opt in opt_chain:
            if (opt['ask'] <= 0.01 or opt['bid'] <= 0.01):
                blacklist_strikes.append(opt['strike'])
        for opt in opt_chain:
            if opt['strike'] not in blacklist_strikes:
                if opt['option_type'] == 'call':
                    calls_chain.append({'symbol':opt['symbol'],'ask':opt['ask'],'bid':opt['bid'],'strike':opt['strike'],'expiration_date':opt['expiration_date'],'option_type':opt['option_type']})
                else:
                    puts_chain.append({'symbol':opt['symbol'],'ask':opt['ask'],'bid':opt['bid'],'strike':opt['strike'],'expiration_date':opt['expiration_date'],'option_type':opt['option_type']})

        calls_chain = tuple(calls_chain)
        puts_chain = tuple(puts_chain)
        combs_prelim = list(itertools.combinations_with_replacement(['Buy Call', 'Sell Call', 'Buy Put', 'Sell Put', None], 4))
        every_strategy = []
        for o in combs_prelim:
            if o.count('Buy Call') >= o.count('Sell Call') and o.count('Buy Put') >= o.count('Sell Put') and o.count(None) < len(o):
                every_strategy.append(o)
        now = datetime.datetime.now()
        print('Startup Time:', now - startTime)
        opt_comb_start_time = now
        next_start = now
        setup_time = 0
        result_time = 0
        loop_time = 0
        cnt2 = 0
        strategy_summaries = []

        for strategy in strategy_types:
            this_strategies_options_chains = [calls_chain if opt[-4:] == 'Call' else puts_chain for opt in strategy if opt]
            option_iterator_for_strategy = itertools.product( *this_strategies_options_chains )
            cnt2 += 1
            now = datetime.datetime.now()
            print((now - next_start), '  On to', cnt2, '/', len(strategy_types), strategy)
            begin = now
            next_start = now
            this_summary = []
            for opt_comb in (option_iterator_for_strategy):
                now = datetime.datetime.now()
                loop_time += (now - begin).total_seconds()
                begin = now
                # TODO: Figure out how to make this filter shorter on time. It's taking way to long
                cnt += 1
                strat = Strat(stock)
                for idx, option in enumerate(opt_comb):
                    strat.add_option(option, 'buy' if strategy[idx][:4] == 'Buy ' else 'sell')
                # strat.add_option(opt_comb[0], 'buy' if strategy[0][:4] == 'Buy ' else 'sell')
                # strat.add_option(opt_comb[1], 'buy' if strategy[1][:4] == 'Buy ' else 'sell')
                # strat.add_option(opt_comb[2], 'buy' if strategy[2][:4] == 'Buy ' else 'sell')
                # strat.add_option(opt_comb[3], 'buy' if strategy[3][:4] == 'Buy ' else 'sell')
                # TODO: Continue to optimize this step
                now = datetime.datetime.now()
                setup_time += (now - begin).total_seconds()
                begin = now
                ret_val = strat.test_results(stock_close_prices)
                if ret_val:
                    this_summary.append(strat)
                    this_summary.sort(key=lambda k:k.annualized_multiplier, reverse=True)
                    this_summary = this_summary[:10]
                    summaries.append(strat)
                    summaries.sort(key=lambda k:k.annualized_multiplier, reverse=True)
                    summaries = summaries[:10]
                else:
                    raise ValueError('strat not valid')
                now = datetime.datetime.now()
                result_time += (now - begin).total_seconds()
                begin = now
            strategy_summaries.append(this_summary)
            for o in this_summary[:3]:
                # Get probability for different time periods
                o.calculate_other_time_probs()
                string = f'{o.avg_multiplier:.2f}({o.win_per_1_yr:.0f}%) {o.avg_3_months:.2f}({o.win_per_3_mo:.0f}%) {o.avg_1_month:.2f}({o.win_per_1_mo:.0f}%) {o.avg_2_weeks:.2f}({o.win_per_2_wk:.0f}%)   '
                for opt in o.options:
                    string += str(opt['strike']) + str(opt['option_type']) + '@' + str(opt['cost']) + '  '
                print(string)

        print('setup time', setup_time)
        print('result_time', result_time)
        print('loop time', loop_time)
        print('Opt comb time:', datetime.datetime.now() - opt_comb_start_time)

        print("IC5 Runtime: ", (datetime.datetime.now() - startTime), '  Strategies Tested: ', cnt)
        self.results_tuples = summaries
        for o in self.results_tuples[:10]:
            string =  '%.3E' % o.annualized_multiplier+ '  '
            for opt in o.options:
                string += str(opt['strike']) + str(opt['option_type']) + '@' + str(opt['cost']) + '  '
            print(string)

        startTime = datetime.datetime.now()
        strat = Strat(stock)
        for opt in self.results_tuples[0].options:
            strat.add_option({'strike':opt['strike'], 'option_type':opt['option_type'], 'cost':opt['cost'], 'expiration_date':opt['expiration_date']}, 'buy' if opt['cost']>0 else 'sell')
        strat.test_results(self.probability_multipliers[opt_comb[0]['expiration_date']])
        print('Top Analysis Time:', datetime.datetime.now() - startTime)
        # strat.show_analysis_plot()

    # def try_ic(self):
    #     startTime = datetime.datetime.now()
    #     avg_perc_gains = []
    #     strats = self.get_strategy_methods()
    #     chains = self.get_option_chains()
    #     stock = self.get_live_stock()
    #
    #     for opt_chain in chains[3:4]:
    #         a = itertools.product(opt_chain[50:70], opt_chain[50:70], opt_chain[50:70], opt_chain[50:70])
    #         while True:
    #             opt_comb = next(a, None)
    #             if opt_comb == None:
    #                 break
    #             if (is_put(opt_comb[0]) and is_put(opt_comb[1]) and is_call(opt_comb[2]) and is_call(opt_comb[3])
    #                 and opt_comb[0]['strike'] <= opt_comb[1]['strike'] and opt_comb[1]['strike'] <= opt_comb[2]['strike']
    #                 and opt_comb[2]['strike'] <= opt_comb[3]['strike']):
    #                 assert(opt_comb[0]['expiration_date'] == opt_comb[1]['expiration_date'])
    #                 assert(opt_comb[0]['expiration_date'] == opt_comb[2]['expiration_date'])
    #                 assert(opt_comb[0]['expiration_date'] == opt_comb[3]['expiration_date'])
    #                 put_buy_strike = opt_comb[0]['strike']
    #                 put_debit = opt_comb[0]['ask']
    #                 option_1_exp = opt_comb[0]['expiration_date']
    #                 put_sell_strike = opt_comb[1]['strike']
    #                 put_credit = opt_comb[1]['bid']
    #                 call_sell_strike = opt_comb[2]['strike']
    #                 call_credit = opt_comb[2]['bid']
    #                 call_buy_strike = opt_comb[3]['strike']
    #                 call_debit = opt_comb[3]['ask']
    #                 stock_cost = stock['ask']
    #                 days_to_exp = self.get_days_to_expirations()[option_1_exp]
    #                 prob_plot = np.array(self.get_probability_plots()[option_1_exp])
    #                 prob_mult = self.probability_multipliers[option_1_exp]
    #                 # stock_close_price = stock_cost * ((prob_plot / 100) + 1)
    #                 stock_close_price = stock_cost * np.array(prob_mult)
    #                 # print(put_buy_strike, put_debit, put_sell_strike, put_credit,
    #                     # call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
    #                 profit_return = strategies.iron_condor_profit_multiplier(put_buy_strike, put_debit, put_sell_strike, put_credit,
    #                     call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
    #                 average_mult_gain = np.mean(profit_return['multiplier'])
    #                 average_profit = np.mean(profit_return['profit'])
    #                 initial_cost = profit_return['initial_cost']
    #
    #                 # annual_percent_gain = annualize_percentage(average_percent_gain, days_to_exp)
    #                 possible_annual_occurances = 252 / days_to_exp
    #                 annualized_multiplier = average_mult_gain ** possible_annual_occurances
    #                 avg_perc_gains.append({'percent':annualized_multiplier,'opt1':opt_comb[0]['symbol'],'opt1ask':opt_comb[0]['ask'],
    #                     'opt2':opt_comb[1]['symbol'],'opt2bid':opt_comb[1]['bid'],
    #                     'opt3':opt_comb[2]['symbol'],'opt3bid':opt_comb[2]['bid'],
    #                     'opt4':opt_comb[3]['symbol'],'opt4bid':opt_comb[3]['ask'],'stock_ask':stock['ask'],'a':initial_cost})
    #
    #     print("IC Runtime: ", (datetime.datetime.now() - startTime), '  Summary Length: ', len(avg_perc_gains))
    #     sor = sorted(avg_perc_gains, key=lambda k:k['percent'], reverse = True)
    #     for o in sor[:3]:
    #         print(o['percent'], o['opt1'], o['opt2'], o['opt3'], o['opt4'])

    def iterate_all_possibilities(self):
        avg_perc_gains = []
        strats = self.get_strategy_methods()
        chains = self.get_option_chains()
        opts = [item for sublist in chains for item in sublist]
        opts.append(None)

        startTime = datetime.datetime.now()
        a = itertools.product(opts, [None], [None], [None])
        print("Runtime itertools: ", (datetime.datetime.now() - startTime))
        print('opts:',len(opts))
        startTime = datetime.datetime.now()
        b = 0
        while True:
            n = (next(a, None))
            if n == None:
                break
            b = b + 1
            if n[0] != None:
                temp = self.get_average_percent_gains('CC', self.get_live_stock(),n[0],n[1],None,None)
                if temp != None:
                    avg_perc_gains.append([temp, n[0],n[1]])

        print("Runtime Next Iter: ", (datetime.datetime.now() - startTime))

        sor = sorted(avg_perc_gains, key=lambda k:k[0], reverse=True)
        for per in sor[:5]:
            print(per)

        print('iter:',b)

        def get_results(self):
            table = BeautifulTable(max_width=200)
            table.set_style(BeautifulTable.STYLE_MARKDOWN)
            table.column_headers = ["Symbol", "Option 1", "Option 2", "Option 3", "Option 4", "Multiplier"]
            for entry in self.results_tuples[:10]:
                table.append_row(entry.get_table_row())

            print(table)

def is_call(option):
    return (option['option_type'] == 'call')

def is_put(option):
    return (option['option_type'] == 'put')

class Strat2(object):

    def __init__(self, name, option_chains):
        self.name = name
        self.option_chains = option_chains
        self.options_list = []

    def add_option(self, opt_ref, expiration_range, strike_range, type, quantity):
        # for # TODO NOW: iterate through all the options and filter the ones that don't meet the criteria
        opt = {'opt_ref':opt_ref, 'expiration_range':expiration_range,
            'strike_range':strike_range, 'type':type, 'quantity':quantity}
        self.options_list.append(opt)


class Strat(object):

    def __init__(self, stock):
        self.stock = stock
        self.stock_ask = self.stock['ask']
        self.purchase_stock = False
        self.stock_cost = 0
        self.options = []
        self.strike_list = []

    def add_option(self, option, buy_or_sell):
        if option == None:
            return
        assert((buy_or_sell == 'buy') or (buy_or_sell == 'sell'))
        option = option.copy()
        option['buy_or_sell'] = buy_or_sell
        if 'cost' not in option:
            option['cost'] = option['ask'] if buy_or_sell == 'buy' else -option['bid']
        self.options.append(option)
        self.strike_list.append(option['strike'])
        assert(option['expiration_date'] == self.options[0]['expiration_date'])

    def test_results(self, stock_close_prices):
        self.get_cost_of_options()
        # for opt in self.options:
        #     cost = opt['cost']
        #     if cost == 0:
        #         raise('Option not available')
        #         return False
        self.get_collateral_of_options()
        # if self.check_if_valid_strat() != True:
        #     print(self.options)
        #     raise("don't work")
        #     return False
        # self.get_cost_of_stock()
        self.stock_close_price = stock_close_prices#np.round_(np.array(close_multiplier_list) * self.stock_ask, decimals=12)
        # print(stock_close_price, close_multiplier_list, self.stock_ask)
        self.front_end_cost = self.collateral + self.cost_of_options + self.stock_cost
        self.stock_end_value = 0 #self.get_value_of_stock_at_expiration(self.stock_close_price)
        self.options_end_value = sum(self.get_value_of_options_at_expiration(self.stock_close_price))
        self.back_end_value = self.stock_end_value + self.options_end_value + self.collateral
        self.gain_multipliers = self.back_end_value / self.front_end_cost
        self.avg_multiplier = np.mean(self.gain_multipliers)
        self.annualize_avg_multiplier()
        # print('Cost:', self.front_end_cost, '| Gain:', self.back_end_value, '| Multiplier:', self.gain_multipliers)
        # self.report = {'cost': front_end_cost, 'gain': back_end_value,
        #     'gain_multipliers': gain_multipliers, 'own_stock': self.purchase_stock,
        #     'stock_ask': self.stock_ask, }
        return True

    def annualize_avg_multiplier(self):
        annual_market_days = 252
        self.days_to_strat_end = 5#np.where(market_calendar.date == (datetime.datetime.strptime(self.options[0]['expiration_date'], '%Y-%m-%d').date()))[0][0] + 1
        possible_annual_occurances = annual_market_days / self.days_to_strat_end
        self.annualized_multiplier = self.avg_multiplier ** possible_annual_occurances

    def calculate_other_time_probs(self):
        self.win_per_1_yr = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers) * 100 / len(self.gain_multipliers)
        self.avg_6_months = np.mean(self.gain_multipliers[-130:])
        self.win_per_6_mo = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-130:]) * 100 / len(self.gain_multipliers[-130:])
        self.avg_3_months = np.mean(self.gain_multipliers[-65:])
        self.win_per_3_mo = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-65:]) * 100 / len(self.gain_multipliers[-65:])
        self.avg_1_month = np.mean(self.gain_multipliers[-21:])
        self.win_per_1_mo = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-21:]) * 100 / len(self.gain_multipliers[-21:])
        self.avg_3_weeks = np.mean(self.gain_multipliers[-15:])
        self.win_per_3_wk = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-15:]) * 100 / len(self.gain_multipliers[-15:])
        self.avg_2_weeks = np.mean(self.gain_multipliers[-10:])
        self.win_per_2_wk = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-10:]) * 100 / len(self.gain_multipliers[-10:])
        self.avg_1_week = np.mean(self.gain_multipliers[-5:])
        self.win_per_1_wk = sum(1 if multiplier > 1 else 0 for multiplier in self.gain_multipliers[-5:]) * 100 / len(self.gain_multipliers[-5:])

    def get_report(self):
        self.report = {}
        self.report['stock'] = self.stock
        self.report['option_0'] = {'description':None}
        self.report['option_1'] = {'description':None}
        self.report['option_2'] = {'description':None}
        self.report['option_3'] = {'description':None}
        for idx, opt in enumerate(self.options):
            self.report['option_'+str(idx)] = opt
            # self.report['option'+str(idx)+'_exp'] = opt['expiration_date']
            # self.report['option'+str(idx)+'_strike'] = opt['strike']
            # self.report['option'+str(idx)+'_type'] = opt['option_type']
            # self.report['option'+str(idx)+'_act'] = opt['buy_or_sell']
            # self.report['option'+str(idx)+'_cost'] = opt['cost']
            # self.report['option'+str(idx)+'_description'] = opt['description']
        self.report['calculations'] = {}
        self.report['calculations']['cost'] = self.front_end_cost
        self.report['calculations']['gain'] = self.back_end_value
        self.report['calculations']['gain_multipliers'] = self.gain_multipliers
        self.report['calculations']['own_stock'] = self.purchase_stock
        return(self.report)

    def check_if_valid_strat(self):
        if self.collateral >= 1e5:
            self.purchase_stock = True
            self.get_collateral_of_options()
        if self.collateral >= 1e5:
            return False
        else:
            return True

    def get_cost_of_stock(self):
        if self.purchase_stock == True:
            self.stock_cost = self.stock['ask']
        else:
            self.stock_cost = 0
        return self.stock_cost

    def get_value_of_stock_at_expiration(self, stock_price_at_expiration):
        if self.purchase_stock == True:
            stock_value = stock_price_at_expiration
        else:
            stock_value = 0
        return stock_value

    def get_cost_of_options(self):
        self.options_cost = np.array([opt['cost'] for opt in self.options])
        self.cost_of_options = np.sum(self.options_cost)
        return self.cost_of_options

    def get_value_of_options_at_expiration(self, stock_price_at_expiration):
        gain_list = []
        price = np.array(stock_price_at_expiration)
        for opt in self.options:
            if opt['option_type'] == 'call' and opt['buy_or_sell'] == 'buy':
                gain = np.maximum((price - opt['strike']), 0)
            elif opt['option_type'] == 'call' and opt['buy_or_sell'] == 'sell':
                gain = - np.maximum((price - opt['strike']), 0)
            elif opt['option_type'] == 'put' and opt['buy_or_sell'] == 'buy':
                gain = np.maximum((opt['strike'] - price), 0)
            elif opt['option_type'] == 'put' and opt['buy_or_sell'] == 'sell':
                gain = - np.maximum((opt['strike'] - price), 0)
            else:
                raise('option type error or buy_or_sell option not avialable')
            gain_list.append((gain))
        return gain_list

    def get_total_value_at_expiration(self, stock_price_at_expiration):
        return np.sum(self.get_value_of_options_at_expiration(stock_price_at_expiration)) + self.get_value_of_stock_at_expiration(stock_price_at_expiration)

    def get_collateral_of_options(self):
        test_price_points = [0, 1e10] + self.strike_list
        gain_for_each_price = []
        for price in test_price_points:
            gain_for_each_price.append(self.get_total_value_at_expiration(price))
        min_value = min(gain_for_each_price)
        self.collateral = max(-min_value,0)
        return self.collateral

    def get_table_row(self):
        stock_str = ('Buy ' if self.purchase_stock else '') + self.stock['symbol'] + ' ${:,.2f}'.format(self.stock['ask'])
        row = [stock_str]

        for opt in self.options:
            exp = opt['expiration_date']
            strike = '${:,.2f}'.format(opt['strike'])
            act = 'Buy ' if opt['buy_or_sell'] == 'buy' else 'Sell'
            type = 'Call' if opt['option_type'] == 'call' else 'Put '
            price = '${:,.2f}'.format(abs(opt['cost']))
            opt_str = exp + ' ' + strike + ' ' + act + ' ' + type + ' ' + price
            row.append(opt_str)
        while len(row) < 5:
            row.append('None')
        row.append('{:,.3f}x'.format(np.mean(self.gain_multipliers)))
        return row

    def get_detailed_table_row(self):
        row = get_table_row()
        front_end_str = '{:,.2f} + {:,.2f} + {:,.2f} = {:,.2f}'.format(self.collateral, self.cost_of_options, self.stock_cost, self.front_end_cost)
        back_end_str = self.back_end_value
        row.append(front_end_str)
        row.append(back_end_str)
        return row

    def get_detailed_tuple_summary(self):
        StockTuple = namedtuple('StockTuple', 'symbol ask purchase')
        OptTuple = namedtuple('OptTuple', 'expiration strike type cost')
        OptionsTuple = namedtuple('OptionsTuple', 'option_1 option_2 option_3 option_4')
        ResultTuple = namedtuple('ResultTuple', 'annualized_multiplier avg_multiplier multipliers stock_close front_end_cost stock_cost options_cost collateral back_end_value stock_end_value options_end_value')
        StrategyTuple = namedtuple('StrategyTuple', 'stock options results')

        s = StockTuple( self.stock['symbol'] , self.stock['ask'] , self.purchase_stock )
        o = tuple([OptTuple( opt['expiration_date'] , opt['strike'] , opt['option_type'] , opt['cost'] ) for opt in self.options])
        r = ResultTuple( self.annualized_multiplier , self.avg_multiplier , self.gain_multipliers ,
            self.stock_close_price , self.front_end_cost , self.stock_cost , self.options_cost ,
            self.collateral , self.back_end_value , self.stock_end_value , self.options_end_value)
        reportTuple = StrategyTuple( s , o , r )
        return reportTuple

    def show_analysis_plot(self):
        # Data for plotting
        gain_multipliers = []
        strike_list_sorted = sorted(self.strike_list + [self.stock['ask']])
        stock_close_prices = test_price_points = [strike_list_sorted[0]*0.9] + strike_list_sorted + [strike_list_sorted[-1]*1.1]
        for price in stock_close_prices:
            front_end_cost = self.collateral + self.cost_of_options + self.stock_cost
            stock_end_value = self.get_value_of_stock_at_expiration(price)
            options_end_value = sum(self.get_value_of_options_at_expiration(price))
            back_end_value = stock_end_value + options_end_value + self.collateral
            gain_multipliers.append(back_end_value / front_end_cost)

        t = np.array(stock_close_prices)
        s = np.array(gain_multipliers)
        # plt.show()

        fig, ax = plt.subplots()
        his = ax.twinx()
        n, bins, patches = his.hist(self.stock_close_price, 16, facecolor='yellow')
        ax.plot(t, s)
        ax.set_zorder(his.get_zorder()+1)
        ax.patch.set_visible(False)

        ax.set(xlabel='Stock Clost Price (USD)', ylabel='Gain Multiplier',
               title='Top Strategy Analysis')
        ax.grid()

        fig.savefig("test.png")
        plt.show()


startTime = datetime.datetime.now()
today_dt = datetime.datetime.now()
delta_dt = datetime.timedelta(days=(366*5))
market_calendar = (mcal.get_calendar('NYSE').valid_days(start_date=(today_dt).strftime('%Y-%m-%d'),
    end_date=(today_dt + delta_dt).strftime('%Y-%m-%d')))
print("Runtime: ", (datetime.datetime.now() - startTime))

if __name__ == '__main__':

    def test_Strat():
        a = Strat({'symbol':'BAC','ask':100})
        a.add_option({'ask':0.50, 'bid':0.30, 'strike':103, 'option_type':'call', 'expiration_date':'2019-03-01',
            'description': 'BAC Mar 1 2019 $103.00 Call'}, 'sell')
        a.add_option({'ask':1.1, 'bid':0.9, 'strike':99, 'option_type':'put', 'expiration_date':'2019-03-01',
            'description': 'BAC Mar 1 2019 $99.00 Put'}, 'sell')
        a.add_option({'ask':0.25, 'bid':0.22, 'strike':105, 'option_type':'call', 'expiration_date':'2019-03-01',
            'description': 'BAC Mar 1 2019 $105.00 Call'}, 'buy')
        a.add_option({'ask':0.6, 'bid':0.55, 'strike':95, 'option_type':'put', 'expiration_date':'2019-03-01',
            'description': 'BAC Mar 1 2019 $95.00 Put'}, 'buy')
        mult = [1.05, 0.99, 0.96]
        a.test_results(np.array(mult) * 100)
        summary = a.get_detailed_tuple_summary()
        assert(summary.stock == ('BAC', 100, False))
        assert(summary.options[0] == ('2019-03-01', 103, 'call', -0.30))
        assert(summary.options[1] == ('2019-03-01', 99, 'put', -0.90))
        assert(summary.options[2] == ('2019-03-01', 105, 'call', 0.25))
        assert(summary.options[3] == ('2019-03-01', 95, 'put', 0.60))
        assert(list(summary.results.stock_close) == [105, 99, 96])
        assert(summary.results.stock_cost == 0)
        assert(list(summary.results.options_cost) == [-0.3, -0.9, 0.25, 0.6])
        assert(summary.results.collateral == 4)
        assert(summary.results.front_end_cost == 3.65)
        assert(summary.results.stock_end_value == 0)
        assert(list(summary.results.options_end_value) == [-2, 0, -3])
        assert(list(summary.results.back_end_value) == [2, 4, 1])
        assert(list(summary.results.multipliers) == [0.54794520547945205479452054794521, 1.0958904109589041095890410958904, 0.2739726027397260273972602739726])
        assert(round(summary.results.avg_multiplier,11) == (0.63926940639))


        b = Strat({'symbol':'BAC','ask':50})
        b.add_option({'ask':0.50, 'bid':0.30, 'strike':53, 'option_type':'call', 'expiration_date':'2019-03-22',
            'description': 'BAC Mar 1 2019 $53.00 Call'}, 'buy')
        mult = [1.1,1.05,0.95]
        b.test_results(np.round_(np.array(mult) * 50, 12))
        summary = b.get_detailed_tuple_summary()
        assert(summary.stock == ('BAC', 50, False))
        assert(summary.options[0] == ('2019-03-22', 53, 'call', 0.50))
        assert(len(summary.options) == 1)
        assert(list(summary.results.stock_close) == [55., 52.5, 47.5])
        assert(summary.results.stock_cost == 0)
        assert(list(summary.results.options_cost) == [0.50])
        assert(summary.results.collateral == 0)
        assert(summary.results.front_end_cost == 0.50)
        assert(summary.results.stock_end_value == 0)
        assert(list(summary.results.options_end_value) == [2, 0, 0])
        assert(list(summary.results.back_end_value) == [2, 0, 0])
        assert(list(summary.results.multipliers) == [4, 0, 0])
        assert(round(summary.results.avg_multiplier,11) == (1.33333333333))

        # TODO: Implement ability to add stock purchase
        # c = Strat({'symbol':'BAC','ask':50})
        # c.add_option({'ask':0.50, 'bid':0.30, 'strike':53, 'option_type':'call', 'expiration_date':'2019-03-01',
        #     'description': 'BAC Mar 1 2019 $53.00 Call'}, 'sell')
        # c.add_option({'ask':0.70, 'bid':0.65, 'strike':49, 'option_type':'put', 'expiration_date':'2019-03-01',
        #     'description': 'BAC Mar 1 2019 $49.00 Put'}, 'buy')
        # mult = [1.1,1.05,0.95]
        # c.test_results(np.array(mult) * 50)
        # summary = c.get_detailed_tuple_summary()
        # assert(summary.stock == ('BAC', 50, True))
        # assert(summary.options[0] == ('2019-03-01', 53, 'call', -0.30))
        # assert(summary.options[1] == ('2019-03-01', 49, 'put', 0.70))
        # assert(len(summary.options) == 2)
        # assert(list(summary.results.stock_close) == [55., 52.5, 47.5])
        # assert(summary.results.stock_cost == 50)
        # assert(list(summary.results.options_cost) == [-0.30, 0.70])
        # assert(summary.results.collateral == 0)
        # assert(summary.results.front_end_cost == 50.40)
        # assert(list(summary.results.stock_end_value) == [55, 52.5, 47.5])
        # assert(list(summary.results.options_end_value) == [-2, 0, 1.5])
        # assert(list(summary.results.back_end_value) == [53, 52.5, 49])
        # assert(list(summary.results.multipliers) == [1.0515873015873015873015873015873, 1.0416666666666666666666666666667, 0.97222222222222222222222222222222])
        # assert(summary.results.avg_multiplier == 1.021825396825397)

    def test_update_stock_historicals():
        sao = StockAndOption('BAC')
        sao.update_stock_historicals()
        hist = sao.get_stock_historicals()
        hist_len = len(hist)
        assert hist_len < 255 and hist_len >= 250, ('hist_len:' + str(hist_len))
        assert(isinstance(hist[5]["open"], float))
        assert(isinstance(hist[5]["close"], float))

    def test_update_probability_plots():
        sao = StockAndOption('BAC')
        sao.option_expirations = ['2019-02-01', '2019-02-06', '2019-02-08']
        sao.days_to_expirations = {'2019-02-01':1,'2019-02-06':4,'2019-02-08':6}
        sao.stock_historicals = [{'open':100,'close':101},{'open':102,'close':101},{'open':101.5,'close':102},{'open':102,'close':105},{'open':106,'close':103},{'open':103,'close':100},{'open':99,'close':101},{'open':101,'close':97},{'open':97,'close':99},{'open':98,'close':101}]
        sao.update_probability_plots()
        prob_plots = sao.get_probability_plots()
        assert(round(np.mean(prob_plots['2019-02-01']),9) == 0.089347125)
        assert(round(np.mean(prob_plots['2019-02-06']),9) == -0.975955954)
        assert(round(np.mean(prob_plots['2019-02-08']),9) == -2.614409459)

    def test_get_average_percent_gains():
        sao = StockAndOption('BAC')
        sao.option_expirations = ['2019-02-01', '2019-02-06', '2019-02-08']
        sao.days_to_expirations = {'2019-02-01':1,'2019-02-06':4,'2019-02-08':6}
        sao.probability_plots = {'2019-02-01': [1.0, -0.9803921568627451, 0.49261083743842365, 2.941176470588235, -2.8301886792452833, -2.912621359223301, 2.0202020202020203, -3.9603960396039604, 2.0618556701030926, 3.061224489795918], '2019-02-06': [5.0, 0.9803921568627451, -1.477832512315271, -0.9803921568627451, -8.49056603773585, -3.8834951456310676, 2.0202020202020203], '2019-02-08': [0.0, -0.9803921568627451, -4.433497536945813, -2.941176470588235, -4.716981132075472]}
        option_1 = {'option_type':'call','strike':152,'expiration_date':'2019-02-06','bid':0.75}
        avg_annual_gain = sao.get_average_percent_gains('strategy', {'ask':150}, option_1, None, None, None)
        assert(round(avg_annual_gain,5) == -50.29227)

    def test_iron_condor_profit():
        put_buy_strike = 98
        put_debit = .6
        put_sell_strike = 99
        put_credit = .8
        call_sell_strike = 101
        call_credit = .7
        call_buy_strike = 102
        call_debit = .5
        stock_close_price = 100

        result = strategies.iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
        assert(round(result['percent'],3) == 166.667)
        assert(result['initial_cost'] == 0.6)
        assert(result['profit'] == 1)

        stock_close_price = 105
        result = strategies.iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
        assert(round(result['percent'],3) == 0)
        assert(result['initial_cost'] == 0.6)
        assert(result['profit'] == 0)

        stock_close_price = 98.50
        result = strategies.iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
        assert(round(result['percent'],3) == 83.333)
        assert(result['initial_cost'] == 0.6)
        assert(result['profit'] == 0.5)

        stock_close_price = 98
        result = strategies.iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
        assert(round(result['percent'],3) == 0)
        assert(result['initial_cost'] == 0.6)
        assert(result['profit'] == 0)

        stock_close_price = 101.35
        result = strategies.iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price)
        assert(round(result['percent'],3) == 108.333)
        assert(result['initial_cost'] == 0.6)
        assert(round(result['profit'],10) == 0.65)

    test_Strat()
    test_update_stock_historicals()
    test_update_probability_plots()
    test_get_average_percent_gains()
    test_iron_condor_profit()


    a = StockAndOption('BAC')
    a.update_live_stock()
    a.update_option_chains()
    a.update_stock_historicals()
    a.update_days_to_expirations()
    a.update_probability_plots()
    a.set_strategy_method('Covered Call')
    # a.try_cc()
    # a.try_ic()
    # a.try_ic2()
    # a.try_ic3()
    # a.try_ic4()
    a.try_ic5('2019-03-22')

    # a.try_ic()

    # a.probability_plots = {'2019-02-01': [0,.5,1,1.5,-.5,-1,-1.5]}
    # live_stock = {'ask': 100}
    # option_1 = {'option_type':'call', 'strike': 101, 'expiration':'2019-02-01', 'bid':0.1}
    # print(a.get_average_percent_gains('covered_call',live_stock, option_1, None, None, None))
    # avg_gain = ((100+100.5+101+101+99.5+99+98.5) / 7)
    # print((avg_gain - 99.9)*100/99.9)
    exit()
    # startTime = datetime.datetime.now()
    # # probability_array_for_year_for_x_days('SPY', 5)
    # get_probability_for_exp('SPY', '2019-01-23')
    # print("Runtime: ", (datetime.datetime.now() - startTime))
    # startTime = datetime.datetime.now()
    # # probability_array_for_year_for_x_days('GPRO', 1)
    # get_probability_for_exp('GPRO', '2019-01-27')
    # print("Runtime: ", (datetime.datetime.now() - startTime))
    # startTime = datetime.datetime.now()
    # # probability_array_for_year_for_x_days('BAC', 20)
    # get_probability_for_exp('BAC', '2019-02-02')
    # print("Runtime: ", (datetime.datetime.now() - startTime))
    # startTime = datetime.datetime.now()
    # get_probability_for_exp2('BAC', '2019-02-04')
    # print("Runtime: ", (datetime.datetime.now() - startTime))
    # exit()
