import unittest
from live_data import *
from strategies import *
from sample_data import get_sample_stock_data, get_sample_option_chains
import numpy as np

class MyTests(unittest.TestCase):
    # def test_format(self):
    #     # Test nominal case
    #
    #     # Test possitive boundary
    #
    #     # Test negative boundary
    #
    #     # Test unique cases
    #
    #     # Test failures
    def test_get_average_percent_gains(self):
        print('Testing get_average_percent_gains')
        strategy = [covered_call_profit]
        live_stock = {'ask':100}
        option_1 = {'option_type':'call', 'strike':102, 'expiration_date':'2019-02-01', 'bid':0.5}
        a = StockAndOption('BAC')
        a.days_to_expirations = {'2019-02-01':5}
        a.probability_plots = {'2019-02-01':[2.5,-3,1.25,.25,-.1]}
        per = a.get_average_percent_gains(strategy, live_stock, option_1, None, None, None)
        self.assertAlmostEqual(per['annual_percent'], (1.3403569894282771232909814573065-1)*100)


    def test_percent_change_to_total_decimal(self):
        # Test nominal case
        self.assertEqual(percent_change_to_total_decimal(8.5124), 1.085124)
        self.assertEqual(percent_change_to_total_decimal(-0.1), 0.999)
        # Test possitive boundary
        self.assertEqual(percent_change_to_total_decimal(9999.99999), 100.9999999)
        # Test negative boundary
        self.assertEqual(percent_change_to_total_decimal(-9999.99999), -98.9999999)
        # Test failures
        with self.assertRaises(TypeError):
            percent_change_to_total_decimal('8.1')
        with self.assertRaises(TypeError):
            percent_change_to_total_decimal([8.1,0.999])
        print('Passing test_percent_change_to_total_decimal')

    # def test_strategies_with_chains(self):
    #     sample_stock_data = get_sample_stock_data()
    #     sample_option_chains = get_sample_option_chains()
    #     bac_set = OptionSet('BAC')
    #     self.assertEqual(bac_set.set_stock_data(sample_stock_data), 'BAC')
    #     self.assertEqual(bac_set.set_option_chains(sample_option_chains), ['2019-01-25', '2019-02-01', '2019-02-08'])









    def test_covered_call_profit(self):
        # Test nominal case
        strike = 250
        stock = 245
        credit = 0.5
        close = 249.10

        startTime = datetime.datetime.now()
        for i in range(10000):
            self.assertAlmostEqual(covered_call_profit(strike, stock, credit, close)['percent'], 1.8813905930470348)
        print("Runtime 1: ", (datetime.datetime.now() - startTime))

        # Test with numpy array
        prob_plot = np.array([1,2.5,-3,-0.25,0.5])
        prob_plot = np.tile(1.67346938775510204081632653061,10000)
        close_arr = stock * ((prob_plot / 100) + 1)

        startTime = datetime.datetime.now()
        per = (covered_call_profit(strike, stock, credit, close_arr)['percent'])
        print("Runtime 2: ", (datetime.datetime.now() - startTime))

        print(np.mean(per))
        # Test possitive boundary

        # Test negative boundary

        # Test unique cases

        # Test failures





if __name__ == '__main__':
    unittest.main()
