import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import numpy as np
import Helper

class CoveredCall():

    def __init__(self, strike_price, expiration_date):
        self.strike_price = strike_price
        self.expiration_date = expiration_date

    def credit_gain (self, contract_credit, share_cost):
        return contract_credit / share_cost

    def max_gain (self, contract_credit, share_cost):
        return (0)

class ProtectivePut():

    def __init__ (self, strike_price, expiration_date):
        self.strike_price = strike_price
        self.expiration_date = expiration_date

    def debit_loss (self, contract_debit, share_cost):
        return (contract_debit / share_cost)

    def max_loss (self, contract_debit, share_cost):
        return ((contract_debit + share_cost - self.strike_price) / (share_cost + contract_debit))

def covered_call_profit (call_strike, stock_cost, call_credit, stock_close_price):
    initial_cost = stock_cost - call_credit
    profit = (stock_close_price
            + call_sell_profit(call_strike, stock_close_price)
            - initial_cost)
    percent = profit * 100 / initial_cost
    # print('initial_cost:', initial_cost, 'profit:', profit,'percent:',percent)
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def protective_put_profit (put_strike, stock_cost, put_debit, stock_close_price):
    initial_cost = stock_cost + put_debit
    profit = round((stock_close_price
            + put_buy_profit(put_strike, stock_close_price)
            - initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def collar_profit (stock_cost, put_strike, put_debit, call_strike, call_credit, stock_close_price):
    initial_cost = stock_cost + put_debit - call_credit
    profit = round((stock_close_price
            + put_buy_profit(put_strike, stock_close_price)
            + call_sell_profit(call_strike, stock_close_price)
            - initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def cash_secured_put_profit (put_strike, put_credit, stock_close_price):
    initial_cost = put_strike - put_credit
    profit = round((stock_close_price
            + put_sell_profit(put_strike, stock_close_price)
            - initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def long_call_profit (call_strike, call_debit, stock_close_price):
    initial_cost = call_debit
    profit = round((call_buy_profit(call_strike, stock_close_price)
            - initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def long_put_profit (put_strike, put_debit, stock_close_price):
    initial_cost = put_debit
    profit = round((put_buy_profit(put_strike, stock_close_price)
            - initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def iron_condor_profit(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price):
    collateral = max(put_sell_strike - put_buy_strike, call_buy_strike - call_sell_strike)
    initial_cost = put_debit - put_credit - call_credit + call_debit + collateral
    profit = (put_buy_profit(put_buy_strike, stock_close_price)
            + put_sell_profit(put_sell_strike, stock_close_price)
            + call_sell_profit(call_sell_strike, stock_close_price)
            + call_buy_profit(call_buy_strike, stock_close_price)
            + collateral)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}

def iron_condor_profit_multiplier(put_buy_strike, put_debit, put_sell_strike, put_credit, call_sell_strike, call_credit, call_buy_strike, call_debit, stock_close_price):
    collateral = max(put_sell_strike - put_buy_strike, call_buy_strike - call_sell_strike)
    initial_cost = put_debit - put_credit - call_credit + call_debit + collateral
    profit = (put_buy_profit(put_buy_strike, stock_close_price)
            + put_sell_profit(put_sell_strike, stock_close_price)
            + call_sell_profit(call_sell_strike, stock_close_price)
            + call_buy_profit(call_buy_strike, stock_close_price)
            + collateral)
    mult = profit / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "multiplier":mult}


def call_sell_profit (call_strike, stock_close_price):
    return np.minimum(0, call_strike - stock_close_price)

def call_buy_profit (call_strike, stock_close_price):
    return np.maximum(0, stock_close_price - call_strike)

def put_sell_profit (put_strike, stock_close_price):
    return np.minimum(0, stock_close_price - put_strike)

def put_buy_profit (put_strike, stock_close_price):
    return np.maximum(0, put_strike - stock_close_price)


def weird (stock_price, strike, put_debit, call_credit, stock_close_price):
    initial_cost = stock_price + put_debit - call_credit
    profit = round((stock_close_price
            + call_sell_profit(strike, stock_close_price)
            + put_buy_profit(strike, stock_close_price)
            -initial_cost), 10)
    percent = profit * 100 / initial_cost
    return {"initial_cost":initial_cost, "profit":profit, "percent":percent}


if __name__ == '__main__':

    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, facecolor='#FFFFCC')
    xaxis = np.linspace(4.0, 6.0, 101)
    y1 = []
    y2 = []
    y3 = []
    y4 = []


    for x in xaxis:
        # y1.append(round(long_call_profit(5.25,0.29,x)["percent"]*100,2))
        # y1.append(round(long_call_profit(5,.29,x)["percent"]*100,2))
        # y2.append(round(long_put_profit(5, .29, x)["percent"]*100,2))
        y3.append(round(weird(277.49, 277, 1.43, 2.3, x)["percent"]*100,2))
        # y1.append(call_sell_profit(5,x)+.3)
        # y2.append(call_buy_profit(5,x)+.2)
        # y3.append(put_sell_profit(5,x)+.1)
        # y4.append(put_buy_profit(5,x))

    ax.plot(xaxis,y3)
    ax.grid(True)

    cursor = Helper.SnaptoCursor(ax, xaxis, y3, 'decimal')
    plt.connect('motion_notify_event', cursor.mouse_move)

    plt.show()
