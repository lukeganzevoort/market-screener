('Buy Call', 'Buy Call', 'Buy Call', 'Buy Call', '', '')
('Buy Call', 'Buy Call', 'Buy Call', 'Sell Call', '', '')
('Buy Call', 'Buy Call', 'Buy Call', 'Buy Put', '', '')
('Buy Call', 'Buy Call', 'Buy Call', 'Sell Put', '', 'Cash')
('Buy Call', 'Buy Call', 'Buy Call', None, '', '')
**('Buy Call', 'Buy Call', 'Sell Call', 'Sell Call', '', '')**
    *Long Butterfly Spread w/Calls (buy,sell2,buy)*
    *Skip Strike Butterfly w/Calls (buy,sell2,skip,buy)*
    *Inverse Skip Strike Butterfly w/Calls (sell,buy2,skip,sell)*
    *Long Condor Spread W/Calls (buy,sell,sell,buy)*
('Buy Call', 'Buy Call', 'Sell Call', 'Buy Put', '', '')
('Buy Call', 'Buy Call', 'Sell Call', 'Sell Put', '', 'Cash')
**('Buy Call', 'Buy Call', 'Sell Call', None, '', '')**
    *Back Spread w/ Calls (sell,buy 2x)*
('Buy Call', 'Buy Call', 'Buy Put', 'Buy Put', '', '')
('Buy Call', 'Buy Call', 'Buy Put', 'Sell Put', '', '')
('Buy Call', 'Buy Call', 'Buy Put', None, '', '')
('Buy Call', 'Buy Call', 'Sell Put', None, '', 'Cash')
('Buy Call', 'Buy Call', None, None, '', '')
('Buy Call', 'Sell Call', 'Sell Call', 'Buy Put', 'Purchase', '')
('Buy Call', 'Sell Call', 'Sell Call', 'Sell Put', 'Purchase', 'Cash')
('Buy Call', 'Sell Call', 'Sell Call', None, 'Purchase', '')
('Buy Call', 'Sell Call', 'Buy Put', 'Buy Put', '', '')
**('Buy Call', 'Sell Call', 'Buy Put', 'Sell Put', '', '')**
    *Iron Butterfly (buy put,sell put and call,buy call)*
    *Iron Condor (buy put, sell put, sell call, buy call)*
('Buy Call', 'Sell Call', 'Buy Put', None, '', '')
('Buy Call', 'Sell Call', 'Sell Put', None, '', 'Cash')
**('Buy Call', 'Sell Call', None, None, '', '')**
    *Long Call Spread (buy,sell)*
    *Short Call Spread (sell,buy)*
('Buy Call', 'Buy Put', 'Buy Put', 'Buy Put', '', '')
('Buy Call', 'Buy Put', 'Buy Put', 'Sell Put', '', '')
('Buy Call', 'Buy Put', 'Buy Put', None, '', '')
('Buy Call', 'Buy Put', 'Sell Put', 'Sell Put', '', 'Cash')
('Buy Call', 'Buy Put', 'Sell Put', None, '', '')
**('Buy Call', 'Buy Put', None, None, '', '')**
    *Long Straddle (same strike)*
    *Long Strangle (put,call)*
**('Buy Call', 'Sell Put', None, None, '', 'Cash')**
    *Long Combination (same strike)*
**('Buy Call', None, None, None, '', '')**
    *Long Call*
('Sell Call', 'Buy Put', 'Buy Put', 'Buy Put', 'Purchase', '')
('Sell Call', 'Buy Put', 'Buy Put', 'Sell Put', 'Purchase', '')
('Sell Call', 'Buy Put', 'Buy Put', None, 'Purchase', '')
('Sell Call', 'Buy Put', 'Sell Put', 'Sell Put', 'Purchase', 'Cash')
('Sell Call', 'Buy Put', 'Sell Put', None, 'Purchase', '')
**('Sell Call', 'Buy Put', None, None, 'Purchase', '')**
    *Collar*
('Sell Call', 'Sell Put', None, None, 'Purchase', 'Cash')
**('Sell Call', None, None, None, 'Purchase', '')**
    *Covered Call*
('Buy Put', 'Buy Put', 'Buy Put', 'Buy Put', '', '')
('Buy Put', 'Buy Put', 'Buy Put', 'Sell Put', '', '')
('Buy Put', 'Buy Put', 'Buy Put', None, '', '')
**('Buy Put', 'Buy Put', 'Sell Put', 'Sell Put', '', '')**
    *Long Butterfly Spread w/Puts (buy,sell2,buy)*
    *Skip Strike Butterfly w/Puts (buy,skip,sell2,buy)*
    *Inverse Skip strike Butterfly w/Puts (sell,skip,buy2,sell)*
    *Long Condor Spread w/Puts (buy,sell,sell,buy)*
**('Buy Put', 'Buy Put', 'Sell Put', None, '', '')**
    *Back Spread w/ Puts (buy 2x,sell)*
('Buy Put', 'Buy Put', None, None, '', '')
('Buy Put', 'Sell Put', 'Sell Put', None, '', 'Cash')
**('Buy Put', 'Sell Put', None, None, '', '')**
    *Long Put Spread (sell,buy)*
    *Short Put Spread (buy,sell)*
**('Buy Put', None, None, None, '', '')**
    *Long Put*
**('Buy Put', None, None, None, 'Purchase', '')**
    *Protective Put*
**('Sell Put', None, None, None, '', 'Cash')**
    *Cash-Secured Put*
(None, None, None, None, '', '')
**('Buy Call', 'Buy Call', 'Buy Call', 'Sell Call', 'Sell Call', 'Sell Call')**
    *Christmas Tree Butterfly w/Calls (buy,skip,sell3,buy2)*
**('Buy Put', 'Buy Put', 'Buy Put', 'Sell Put', 'Sell Put', 'Sell Put')**
    *Christmas Tree Butterfly w/Calls (buy2,sell3,skip,buy)*

**Calendar Strategies**
    *Fig leaf*
    *Long Calendar Spread w/ Calls*
    *Long Calendar Spread w/ Puts*
    *Diagonal Spread w/ Calls*
    *Diagonal Spread w/ Puts*
    *Double Diagonal*
**Infinite Collateral**
    *Short Call*
    *Short Put*
    *Short Straddle*
    *Short Strangle*
    *Short Combination*
    *Front Spread w/ Calls*
    *Front Spread w/ Puts*
