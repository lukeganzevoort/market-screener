import tradier_data

def set_weekly_change(hist_data):
    for week in range(len(hist_data)-1):
        perc_chg = hist_data[week+1]['open'] / hist_data[week]['open']
        hist_data[week]['change'] = perc_chg

def weekly_winner_score(hist_data, baseline_data):
    for idx, week in reversed(list(enumerate(hist_data))):
        print(idx, week)
        week_rvs = -2-week
        assert(hist_data[week_rvs]['date'] == baseline_data[week_rvs]['date'])
        assert(hist_data[week_rvs]['change']>0)
        assert(baseline_data[week_rvs]['change']>0)


spy_hist = tradier_data.get_historical_data('SPY','2000-01-01','weekly')
set_weekly_change(spy_hist)
dia_hist = tradier_data.get_historical_data('DIA','2000-01-01','weekly')
set_weekly_change(dia_hist)
oneq_hist = tradier_data.get_historical_data('ONEQ','2000-01-01','weekly')
set_weekly_change(oneq_hist)

test_list = ['QQQ', 'AAPL', 'IVV']

aapl_hist = tradier_data.get_historical_data('AAPL','2000-01-01','weekly')
set_weekly_change(aapl_hist)

weekly_winner_score( aapl_hist, spy_hist)

for el in test_list:
    el_hist = tradier_data.get_historical_data(el,'2000-01-01','weekly')
    for start_week in range(len(spy_hist)):
        if spy_hist[start_week]['date'] == el_hist[0]['date']:
            break
    wins = 0
    number_of_weeks = 0
    for week in range(len(el_hist)-1):
        percent_change = el_hist[week+1]['open'] / el_hist[week]['open']
        spy_percent_change = spy_hist[week+start_week+1]['open'] / spy_hist[week+start_week]['open']
        assert el_hist[week+1]['date'] == spy_hist[week+start_week+1]['date']
        assert el_hist[week]['date'] == spy_hist[week+start_week]['date']
        if percent_change > spy_percent_change:
            wins += 1
        number_of_weeks += 1

    print(wins, '/', number_of_weeks, (wins/number_of_weeks))
