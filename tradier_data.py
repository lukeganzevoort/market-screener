import http.client
import json



def get_stock_data(symbol):
    # Request: Market Quotes (https://sandbox.tradier.com/v1/markets/quotes?symbols=spy)
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    # Headers
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/quotes?symbols='+symbol), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # print('Response status ' + str(response.status))
        return (content['quotes']['quote'])
    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_options_chain(symbol, expiration):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/options/chains?symbol='+symbol+'&expiration='+expiration), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['options']['option'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_options_expirations(symbol):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/options/expirations?symbol='+symbol), None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['expirations']['date'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

def get_calendar():
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', '/v1/markets/calendar', None, headers)

    try:
        response = connection.getresponse()
        content = json.loads(response.read())
        # Success
        # print('Response status ' + str(response.status))
        return(content['calendar']['days'])

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'

'''
    \param symbol       A string of the ticker symbol to security to get
                        Historical data.
    \param start_date   A string of the date to start getting historical data.
                        The format for this string is 'YYYY-MM-DD'.
    \param interval     The length between data ('weekly', 'daily', 'monthly')
'''
def get_historical_data(symbol, start_date, interval):
    connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
    headers = {"Accept":"application/json",
               "Authorization":"Bearer 9AQKixnutdGqpXNUK9R4DryxJXbD"}
    connection.request('GET', ('/v1/markets/history?symbol='+symbol+'&interval='+interval+'&start='+start_date), None, headers)

    try:
        response = connection.getresponse()
        read_response = response.read()
        content = json.loads(read_response)
        # Success
        try:
            data_set = content['history']['day']
        except TypeError:
            raise 'Error - Returned None Type from Tradier'
        return data_set

    except http.client.HTTPException:
        # Exception
        print('Exception during request')
        raise 'Error'
