import datetime
import pytz

def is_dst(date, time_zone_name):
    if date.year < 2007:
        raise ValueError()
    time_zone_obj = pytz.timezone(time_zone_name)
    dt = datetime.datetime.combine(date,datetime.time(9,30))
    dst_date = time_zone_obj.localize(dt, is_dst=None)
    return bool(dst_date.dst())

def date_to_epoch_open(market_date):
    epoch_dt = datetime.datetime(1970,1,1)
    daylight_savings_offset = int(is_dst(market_date, 'US/Eastern'))
    open_time = datetime.time(hour=(14-daylight_savings_offset), minute=30)
    utc_datetime = datetime.datetime.combine(market_date,open_time)
    return int((utc_datetime - epoch_dt).total_seconds())

def utc_str_to_datetime(utc_str):
    utc_str = utc_str.replace("T"," ")
    utc_str = utc_str.strip("[A-Z]")
    return datetime.datetime.strptime(utc_str, "%Y-%m-%d %H:%M:%S")

def utc_str_to_epoch(utc_str):
    epoch_dt = datetime.datetime(1970,1,1)
    utc_datetime = utc_str_to_datetime(utc_str)
    return int((utc_datetime - epoch_dt).total_seconds())

def epoch_to_datetime(total_sec):
    seconds_per_day = 60 * 60 * 24
    epoch_dt = datetime.datetime(1970,1,1)
    time_delta = datetime.timedelta(days=int(total_sec/seconds_per_day),
        seconds=int(total_sec%seconds_per_day))
    return (epoch_dt + time_delta)

def datetime_to_epoch(datetime_object):
    epoch_dt = datetime.datetime(1970,1,1)
    return int((datetime_object - epoch_dt).total_seconds())


if __name__ == '__main__':

    def unit_test_is_dst():
        print("START is_dst unit test")
        assert is_dst(datetime.date(2018,7,12), 'US/Eastern')
        assert not is_dst(datetime.date(2018,7,12), 'US/Hawaii')
        assert not is_dst(datetime.date(2018,1,1), 'US/Eastern')
        assert not is_dst(datetime.date(2018,1,1), 'US/Hawaii')
        assert is_dst(datetime.date(2010,7,12), 'US/Eastern')
        assert not is_dst(datetime.date(2010,7,12), 'US/Hawaii')
        assert is_dst(datetime.date(2025,7,12), 'US/Eastern')
        assert not is_dst(datetime.date(2025,7,12), 'US/Hawaii')
        assert is_dst(datetime.date(2018,11,3), 'US/Eastern')
        assert not is_dst(datetime.date(2018,11,4), 'US/Eastern')
        print("COMPLETE is_dst unit test")

    def unit_test_date_to_epoch_open():
        print("START date_to_epoch_open unit test")
        assert date_to_epoch_open(datetime.date(2018,12,13)) == 1544711400
        assert date_to_epoch_open(datetime.date(2018,7,13)) == 1531488600
        assert date_to_epoch_open(datetime.date(2018,3,10)) == 1520692200
        assert date_to_epoch_open(datetime.date(2018,3,11)) == 1520775000
        assert date_to_epoch_open(datetime.date(2018,11,3)) == 1541251800
        assert date_to_epoch_open(datetime.date(2018,11,4)) == 1541341800
        assert date_to_epoch_open(datetime.date(2021,3,13)) == 1615645800
        assert date_to_epoch_open(datetime.date(2021,3,14)) == 1615728600
        assert date_to_epoch_open(datetime.date(2021,11,6)) == 1636205400
        assert date_to_epoch_open(datetime.date(2021,11,7)) == 1636295400
        print("COMPLETE date_to_epoch_open unit test")

    def unit_test_utc_str_to_datetime():
        print("START utc_str_to_datetime unit test")
        assert utc_str_to_datetime("2018-12-13 12:14:53") == datetime.datetime(2018,12,13,12,14,53)
        assert utc_str_to_datetime("2006-7-20T1:14:53") == datetime.datetime(2006,7,20,1,14,53)
        assert utc_str_to_datetime("2020-12-31 23:59:59Z") == datetime.datetime(2020,12,31,23,59,59)
        assert utc_str_to_datetime("2016-2-29T12:14:53Z") == datetime.datetime(2016,2,29,12,14,53)
        print("COMPLETE utc_str_to_datetime unit test")

    def unit_test_utc_str_to_epoch():
        print("START utc_str_to_epoch unit test")
        assert utc_str_to_epoch("2018-12-13 12:14:53") == 1544703293
        assert utc_str_to_epoch("2006-7-20T1:14:53") == 1153358093
        assert utc_str_to_epoch("2020-12-31 23:59:59Z") == 1609459199
        assert utc_str_to_epoch("2016-2-29T12:14:53Z") == 1456748093
        assert utc_str_to_epoch("2018-3-17T12:14:53Z") == 1521288893
        print("COMPLETE utc_str_to_epoch unit test")

    def unit_test_epoch_to_datetime():
        print("START epoch_to_datetime unit test")
        assert epoch_to_datetime(1544703293) == datetime.datetime(2018,12,13,12,14,53)
        assert epoch_to_datetime(1153358093) == datetime.datetime(2006,7,20,1,14,53)
        assert epoch_to_datetime(1609459199) == datetime.datetime(2020,12,31,23,59,59)
        assert epoch_to_datetime(1456748093) == datetime.datetime(2016,2,29,12,14,53)
        assert epoch_to_datetime(1521288893) == datetime.datetime(2018,3,17,12,14,53)
        print("COMPLETE epoch_to_datetime unit test")

    def unit_test_datetime_to_epoch():
        print("START datetime_to_epoch unit test")
        assert datetime_to_epoch(datetime.datetime(2018,12,13,12,14,53)) == 1544703293
        assert datetime_to_epoch(datetime.datetime(2006,7,20,1,14,53)) == 1153358093
        assert datetime_to_epoch(datetime.datetime(2020,12,31,23,59,59)) == 1609459199
        assert datetime_to_epoch(datetime.datetime(2016,2,29,12,14,53)) == 1456748093
        assert datetime_to_epoch(datetime.datetime(2018,3,17,12,14,53)) == 1521288893
        print("COMPLETE datetime_to_epoch unit test")

    unit_test_is_dst()
    unit_test_date_to_epoch_open()
    unit_test_utc_str_to_datetime()
    unit_test_utc_str_to_epoch()
    unit_test_epoch_to_datetime()
    unit_test_datetime_to_epoch()
