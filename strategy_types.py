import itertools

strategies = list(itertools.combinations_with_replacement(['Buy Call', 'Sell Call', 'Buy Put', 'Sell Put', None], 4))
# for combinations in strategies:
#     print(combinations)
every_strategy = []
for o in strategies:
    bc_cnt = o.count('Buy Call')
    sc_cnt = o.count('Sell Call')
    call_cnt = o.count('Buy Call') - o.count('Sell Call')
    bp_cnt = o.count('Buy Put')
    sp_snt = o.count('Sell Put')
    put_cnt = o.count('Buy Put') - o.count('Sell Put')
    a = list(o)
    if call_cnt >= -1 and put_cnt >= -1:
        if call_cnt == -1:
            a.append('Purchase')
        else:
            a.append('')
        if put_cnt == -1:
            a.append('Cash')
        else:
            a.append('')
        every_strategy.append(tuple(a))
        print(tuple(a))
print('Length:', len(every_strategy))
