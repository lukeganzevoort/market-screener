from pandas_datareader import robinhood
from datetime import datetime

def format_tick(open_time, open, close, top, bottom, volume):
    return ({"open_time": open_time,
        "open_price": float(open),
        "close_price": float(close),
        "top_price": float(top),
        "bottom_price": float(bottom),
        "volume": int(volume)})

def format_all_robinhood_ticks(tick_list):
    formated_list = []
    for tick in tick_list:
        formated_list.append(format_tick(
            open_time = tick["begins_at"],
            open = tick["open_price"],
            close = tick["close_price"],
            top = tick["high_price"],
            bottom = tick["low_price"],
            volume = tick["volume"]))
    return formated_list

def get_info(symbol):
    fiveMin = robinhood.RobinhoodHistoricalReader(
        symbol, interval='5minute', span='week')
    daily = robinhood.RobinhoodHistoricalReader(
        symbol, interval='day', span='year')
    weekly = robinhood.RobinhoodHistoricalReader(
        symbol, interval='week', span='5year')

    fiveMinHistory = (fiveMin._get_response(fiveMin.url,fiveMin.params).json()
        ["results"][0]["historicals"])
    dailyHistory = (daily._get_response(daily.url,daily.params).json()
        ["results"][0]["historicals"])
    weeklyHistory = (weekly._get_response(weekly.url,weekly.params).json()
        ["results"][0]["historicals"])

    data = {}
    hist = {}
    hist["5min"] = format_all_robinhood_ticks(fiveMinHistory)
    hist["daily"] = format_all_robinhood_ticks(dailyHistory)
    hist["weekly"] = format_all_robinhood_ticks(weeklyHistory)
    data[symbol] = {"symbol":symbol, "options":0, "historicals":hist}

    return data


if __name__ == "__main__":

    a = get_info("SPY")

    for i in a["SPY"]["historicals"]["5min"]:
        print(i)
