import h5py
import data_getter
import json
from datetime import time as t
import pandas
import market_time
import numpy as np
import datetime
from pathlib import Path
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
from plotly.tools import FigureFactory as FF

class dataset_element:

    def __init__(self, symbol, interval_str, element_data_dict):
        assert(type(symbol) is str)
        assert(symbol.isupper())
        assert((symbol.replace("."," ")).isalpha())
        assert(len(symbol) <= 6)
        self.symbol = symbol
        assert(type(interval_str) is str)
        self.interval_str = interval_str
        self.interval_sec = self.get_interval_in_seconds(self.interval_str)
        assert(type(element_data_dict['open_time']) is str)
        self.time_datetime = market_time.utc_str_to_datetime(element_data_dict['open_time'])
        self.time_epoch = market_time.datetime_to_epoch(self.time_datetime)
        assert(type(element_data_dict) is dict)
        self.open_price = element_data_dict['open_price']
        self.close_price = element_data_dict['close_price']
        self.top_price = element_data_dict['top_price']
        self.bottom_price = element_data_dict['bottom_price']
        self.volume = element_data_dict['volume']

    def get_subset_arr(self):
        subset = []
        subset.append(int(self.time_epoch))
        subset.append(int(self.open_price*100))
        subset.append(int(self.close_price*100))
        subset.append(int(self.top_price*100))
        subset.append(int(self.bottom_price*100))
        subset.append(int(self.volume))
        return (subset)

    @staticmethod
    def get_interval_in_seconds(interval):
        sec_per_min = 60
        sec_per_hour = sec_per_min * 60
        sec_per_day = sec_per_hour * 24
        sec_per_week = sec_per_day * 7

        if interval == "1min":
            return (sec_per_min)
        elif interval == "5min":
            return (5 * sec_per_min)
        elif interval == "10min":
            return (10 * sec_per_min)
        elif interval == "hour":
            return sec_per_hour
        elif interval == "daily":
            return sec_per_day
        elif interval == "weekly":
            return sec_per_week
        else:
            raise ValueError("Interval (\"{!s}\") is not valid.".format(interval))
            return 0

def update_5min_data(symbol):
    recent_data = data_getter.get_info(symbol)
    five_min_data = recent_data["SPY"]["historicals"]["5min"]
    # data_matrix = np.random.uniform(-1, 1, size=(10, 3))
    data=json.dumps(five_min_data)
    write_stock_to_hdf5(symbol, data)

def write_historicals_to_hdf5_2d(file, symbol, data_dict, year):

    set2D = []
    subset = []

    # Format and organize data into data sets
    for set in data_dict:
        subset = []
        subset.append(market_time.utc_str_to_epoch(set['open_time']))
        subset.append(int(set['open_price']*100))
        subset.append(int(set['close_price']*100))
        subset.append(int(set['top_price']*100))
        subset.append(int(set['bottom_price']*100))
        subset.append(int(set['volume']))
        set2D.append(subset)

    with h5py.File(file, 'w') as f:
        gSym = f.create_group(symbol)
        gHist = gSym.create_group('historicals')
        g5min = gHist.create_group('5min')
        gHeader = g5min.create_dataset('header', data=str(['TIME','OPEN','CLOSE','TOP','BOTTOM','VOL']))
        # g2020 = g5min.create_dataset(year, data=set2D, dtype='i4')
        g2018 = g5min.create_dataset(year, shape=(32890,6), dtype='i4')
        g2018[:] = set2D

def read_historicals_from_hdf5_2d(file_name, symbol, year, interval):
    dataset_path = get_dataset_path(symbol, year, interval)
    with h5py.File(file_name, 'r') as f:
        data = f[dataset_path][:]

    return data

def read_historicals_from_hdf5_2d_as_dict(file_name, symbol, year, interval):

    data = read_historicals_from_hdf5_2d(file_name, symbol, year, interval)

    return {data_element[0]: data_element[1:] for data_element in data}

def update_hdf_data(file_name, symbol):
    # Take raw data
    raw_data = data_getter.get_info(symbol)

    for interval_key in raw_data[symbol]["historicals"].keys():
        print(symbol, interval_key)
        # Get each point of the raw data
        for data_point_dict in raw_data[symbol]["historicals"][interval_key]:

            new_elem = dataset_element(symbol, interval_key, data_point_dict)
            subset = new_elem.get_subset_arr()
            create_year_dataset_if_needed(file_name, symbol, new_elem.time_datetime.year, new_elem.interval_str)
            dataset_path = get_dataset_path(symbol, new_elem.time_datetime.year, new_elem.interval_str)
            row_index = calculate_index(file_name, symbol, new_elem.time_datetime.year, new_elem.interval_str, new_elem.time_epoch)
            shp = dataset_shape(new_elem.time_datetime.year, new_elem.interval_str)

            with h5py.File(file_name, 'r+') as f:
                live_dataset = f.require_dataset(dataset_path, shape=shp, dtype='i4')
                if (subset != live_dataset[row_index,].tolist()):
                    print("Write in ", file_name, ":", dataset_path, ":", row_index, " ", new_elem.time_datetime, " ", subset)
                    assert(live_dataset[row_index,1:6].tolist() == [0,0,0,0,0])
                    live_dataset.write_direct(np.asarray(subset),dest_sel=np.s_[row_index,])

            with h5py.File(file_name, 'r') as f:
                live_dataset = f.require_dataset(dataset_path, shape=shp, dtype='i4')
                assert(live_dataset[row_index,].tolist() == subset)

def get_dataset_path(symbol, year, interval):

    assert bool(dataset_element.get_interval_in_seconds(interval))
    return "{!s}/historicals/{!s}/{!s}".format(symbol,interval,str(year))

def is_dataset(file_name, dataset_path):

    with h5py.File(file_name, 'r') as f:
        if dataset_path in f.keys():
            return True

    return False

def create_year_dataset_if_needed(file_name, symbol, year, interval):
    # Create dataset of 488,592-Bytes (20358 Rows)
    dataset_path = get_dataset_path(symbol, year, interval)

    # Create file if it doesn't exist
    my_file = Path(file_name)
    if not my_file.is_file():
        with h5py.File(file_name, 'w-'):
            pass

    if not is_dataset(file_name, dataset_path):

        year_struct = []
        day_duration_sec = (60 * 60 * 6.5)
        interval_sec = dataset_element.get_interval_in_seconds(interval)
        business_days = pandas.bdate_range((str(year) + "-1-1"),(str(year) + "-12-31"))

        if interval_sec >= day_duration_sec:
            if interval == "daily":
                for day in business_days:
                    dt = datetime.datetime.combine(day, datetime.time(0))
                    time_epoch = market_time.datetime_to_epoch(dt)
                    year_struct.append([time_epoch,0,0,0,0,0])
            if interval == "weekly":
                for day in business_days:
                    dt = datetime.datetime.combine(day, datetime.time(0))
                    if dt.isoweekday() == 1:
                        time_epoch = market_time.datetime_to_epoch(dt)
                        year_struct.append([time_epoch,0,0,0,0,0])
        else:
            for day in business_days:
                day_open_epoch = market_time.date_to_epoch_open(day)
                for i in range(int(day_duration_sec/interval_sec)):
                    time_epoch = day_open_epoch + (i * interval_sec)
                    year_struct.append([time_epoch,0,0,0,0,0])

        with h5py.File(file_name, 'a') as f:
            f.create_dataset(dataset_path, data=year_struct, dtype='i4')

def dataset_shape(year, interval):
    business_days_for_year = pandas.bdate_range((str(year) + "-1-1"),str(year) + "-12-31")
    business_days_for_year_number = int(business_days_for_year.get_loc(business_days_for_year[-1])+1)
    shp = (business_days_for_year_number,6)
    if interval == "daily":
        shp = (int(business_days_for_year_number),6)
    elif interval == "weekly":
        cnt = 0
        for day in business_days_for_year:
            if day.isoweekday() == 1:
                cnt = cnt + 1
        shp = (cnt,6)
    elif interval == "5min":
        shp = (int(business_days_for_year_number*6.5*12),6)
    else:
        raise ValueError("Interval (\"{!s}\") is not valid.".format(interval))

    return shp

def calculate_index(file_name, symbol, year, interval, time):
    dataset_path = get_dataset_path(symbol, year, interval)
    day_duration_sec = (60 * 60 * 6.5)
    interval_sec = dataset_element.get_interval_in_seconds(interval)
    dt = market_time.epoch_to_datetime(time)
    business_days = pandas.bdate_range((str(year) + "-1-1"),dt.strftime("%Y-%m-%d"))
    business_day_number = int(business_days.get_loc(business_days[-1]))
    shp = dataset_shape(year, interval)
    if interval == "daily":
        row = business_day_number
    elif interval == "weekly":
        cnt = 0
        for day in business_days:
            if day.isoweekday() == 1:
                cnt = cnt + 1
        row = cnt - 1
    elif interval == "5min":
        day_offset = int(business_day_number*6.5*12)
        day_of_open_epoch = market_time.date_to_epoch_open(dt)
        time_offset = (time - day_of_open_epoch) / interval_sec
        row = int(day_offset + time_offset)
    with h5py.File(file_name, 'r') as f:
        live_dataset = f.require_dataset(dataset_path, shape=shp, dtype='i4')
        row_at_index = live_dataset[row,]
    assert(row_at_index[0] == time)
    return row

def plot_data(file_name, symbol, interval):
    data = read_historicals_from_hdf5_2d(file_name, symbol, "2018", interval)
    # print([x for x in data[:,1] if x is not 0])
    # plt.plot()
    # # plt.ylabel('some numbers')
    # plt.show()
    d = []
    for x in data[:,0]:
        d.append(market_time.epoch_to_datetime(x))
    fig = FF.create_candlestick(data[:,1], data[:,3], data[:,4], data[:,2], dates=d)
    py.iplot(fig, filename='finance/aapl-candlestick', validate=False)

def probability_stock_close_above_threshold_in_week(file_name, symbol, threshold_percent):
    begin = datetime.datetime(2014,1,1)
    begin = datetime.datetime(2018,1,2)
    end = datetime.datetime(2018,12,31)
    begin_sec = market_time.datetime_to_epoch(begin)
    end_sec = market_time.datetime_to_epoch(end)
    range_of_years = range(begin.year, end.year + 1)
    interval = "daily"
    per_chg_list = []

    for year in range_of_years:
        dat = read_historicals_from_hdf5_2d(file_name, symbol, year, interval)
        for row in dat:
            if (row[1] != 0 and row[2] != 0 and row[0] > begin_sec and row[0] < end_sec):
                perc_chg = ((row[2] / row[1]) - 1) * 100
                per_chg_list.append(perc_chg)

    prob_above_threshold = (sum((percent_changed > threshold_percent) for percent_changed in per_chg_list) / len(per_chg_list))
    return prob_above_threshold

# def percent_change_prob(file_name):
#     symbol = "SPY"
#     interval = "weekly"
#     years = ["2018","2017","2016","2015","2014"]
#     per_chg_list = []
#     window = 86400 * 2
#     for year in years:
#         dat = read_historicals_from_hdf5_2d(file_name, symbol, year, interval)
#         for row in dat:
#             if not row[1] == 0:
#                 perc_chg = ((row[2] / row[1]) - 1) * 100
#                 # print(row, "%.2f" % (perc_chg), round(perc_chg,1))
#                 per_chg_list.append(round(perc_chg,1))
#
#
#
#     min_val = round(min(per_chg_list))
#     max_val = round(max(per_chg_list))
#     bins_num = (max_val - min_val)*2+1
#     b = np.linspace(min_val,max_val,bins_num)
#     print(b)
#     n, bins, patches = plt.hist(per_chg_list, bins=b, facecolor='blue', alpha=0.5)
#     s = sum(n)
#     prob = n/s*100
#     print(prob, sum(prob))
#     print(n)
#     print(bins)
#     print(patches)
#     plt.show()


if __name__ == '__main__':
    startTime = datetime.datetime.now()
    file = "market-data.hdf5"
    # percent_change_prob(file)
    print(probability_stock_close_above_threshold_in_week(file,"SPY",0))
    exit()

    symbols = ['A', 'AAL', 'AAP', 'AAPL', 'ABBV', 'ABC', 'ABMD', 'ABT', 'ACN', 'ADBE', 'ADI', 'ADM', 'ADP', 'ADS', 'ADSK', 'AEE', 'AEP', 'AES', 'AET', 'AFL', 'AGN', 'AIG', 'AIV', 'AIZ', 'AJG', 'AKAM', 'ALB', 'ALGN', 'ALK', 'ALL', 'ALLE', 'ALXN', 'AMAT', 'AMD', 'AME', 'AMG', 'AMGN', 'AMP', 'AMT', 'AMZN', 'ANDV', 'ANSS', 'ANTM', 'AON', 'AOS', 'APA', 'APC', 'APD', 'APH', 'APTV', 'ARE', 'ARNC', 'ATVI', 'AVB', 'AVGO', 'AVY', 'AWK', 'AXP', 'AZO', 'BA', 'BAC', 'BAX', 'BBT', 'BBY', 'BDX', 'BEN', 'BF.B', 'BHF', 'BHGE', 'BIIB', 'BK', 'BKNG', 'BLK', 'BLL', 'BMY', 'BR', 'BRK.B', 'BSX', 'BWA', 'BXP', 'C', 'CA', 'CAG', 'CAH', 'CAT', 'CB', 'CBOE', 'CBRE', 'CBS', 'CCI', 'CCL', 'CDNS', 'CELG', 'CERN', 'CF', 'CFG', 'CHD', 'CHRW', 'CHTR', 'CI', 'CINF', 'CL', 'CLX', 'CMA', 'CMCSA', 'CME', 'CMG', 'CMI', 'CMS', 'CNC', 'CNP', 'COF', 'COG', 'COL', 'COO', 'COP', 'COST', 'COTY', 'CPB', 'CPRT', 'CRM', 'CSCO', 'CSX', 'CTAS', 'CTL', 'CTSH', 'CTXS', 'CVS', 'CVX', 'CXO', 'D', 'DAL', 'DE', 'DFS', 'DG', 'DGX', 'DHI', 'DHR', 'DIS', 'DISCA', 'DISCK', 'DISH', 'DLR', 'DLTR', 'DOV', 'DRE', 'DRI', 'DTE', 'DUK', 'DVA', 'DVN', 'DWDP', 'DXC', 'EA', 'EBAY', 'ECL', 'ED', 'EFX', 'EIX', 'EL', 'EMN', 'EMR', 'EOG', 'EQIX', 'EQR', 'EQT', 'ES', 'ESRX', 'ESS', 'ETFC', 'ETN', 'ETR', 'EVHC', 'EVRG', 'EW', 'EXC', 'EXPD', 'EXPE', 'EXR', 'F', 'FAST', 'FB', 'FBHS', 'FCX', 'FDX', 'FE', 'FFIV', 'FIS', 'FISV', 'FITB', 'FL', 'FLIR', 'FLR', 'FLS', 'FLT', 'FMC', 'FOX', 'FOXA', 'FRT', 'FTI', 'FTV', 'GD', 'GE', 'GGP', 'GILD', 'GIS', 'GLW', 'GM', 'GOOG', 'GOOGL', 'GPC', 'GPN', 'GPS', 'GRMN', 'GS', 'GT', 'GWW', 'HAL', 'HAS', 'HBAN', 'HBI', 'HCA', 'HCP', 'HD', 'HES', 'HFC', 'HIG', 'HII', 'HLT', 'HOG', 'HOLX', 'HON', 'HP', 'HPE', 'HPQ', 'HRB', 'HRL', 'HRS', 'HSIC', 'HST', 'HSY', 'HUM', 'IBM', 'ICE', 'IDXX', 'IFF', 'ILMN', 'INCY', 'INFO', 'INTC', 'INTU', 'IP', 'IPG', 'IPGP', 'IQV', 'IR', 'IRM', 'ISRG', 'IT', 'ITW', 'IVZ', 'JBHT', 'JCI', 'JEC', 'JEF', 'JNJ', 'JNPR', 'JPM', 'JWN', 'K', 'KEY', 'KHC', 'KIM', 'KLAC', 'KMB', 'KMI', 'KMX', 'KO', 'KORS', 'KR', 'KSS', 'KSU', 'L', 'LB', 'LEG', 'LEN', 'LH', 'LKQ', 'LLL', 'LLY', 'LMT', 'LNC', 'LNT', 'LOW', 'LRCX', 'LUV', 'LYB', 'M', 'MA', 'MAA', 'MAC', 'MAR', 'MAS', 'MAT', 'MCD', 'MCHP', 'MCK', 'MCO', 'MDLZ', 'MDT', 'MET', 'MGM', 'MHK', 'MKC', 'MLM', 'MMC', 'MMM', 'MNST', 'MO', 'MOS', 'MPC', 'MRK', 'MRO', 'MS', 'MSCI', 'MSFT', 'MSI', 'MTB', 'MTD', 'MU', 'MYL', 'NBL', 'NCLH', 'NDAQ', 'NEE', 'NEM', 'NFLX', 'NFX', 'NI', 'NKE', 'NKTR', 'NLSN', 'NOC', 'NOV', 'NRG', 'NSC', 'NTAP', 'NTRS', 'NUE', 'NVDA', 'NWL', 'NWS', 'NWSA', 'O', 'OKE', 'OMC', 'ORCL', 'ORLY', 'OXY', 'PAYX', 'PBCT', 'PCAR', 'PCG', 'PEG', 'PEP', 'PFE', 'PFG', 'PG', 'PGR', 'PH', 'PHM', 'PKG', 'PKI', 'PLD', 'PM', 'PNC', 'PNR', 'PNW', 'PPG', 'PPL', 'PRGO', 'PRU', 'PSA', 'PSX', 'PVH', 'PWR', 'PX', 'PXD', 'PYPL', 'QCOM', 'QRVO', 'RCL', 'RE', 'REG', 'REGN', 'RF', 'RHI', 'RHT', 'RJF', 'RL', 'RMD', 'ROK', 'ROP', 'ROST', 'RSG', 'RTN', 'SBAC', 'SBUX', 'SCG', 'SCHW', 'SEE', 'SHW', 'SIVB', 'SJM', 'SLB', 'SLG', 'SNA', 'SNPS', 'SO', 'SPG', 'SPGI', 'SRCL', 'SRE', 'STI', 'STT', 'STX', 'STZ', 'SWK', 'SWKS', 'SYF', 'SYK', 'SYMC', 'SYY', 'T', 'TAP', 'TDG', 'TEL', 'TGT', 'TIF', 'TJX', 'TMK', 'TMO', 'TPR', 'TRIP', 'TROW', 'TRV', 'TSCO', 'TSN', 'TSS', 'TTWO', 'TWTR', 'TXN', 'TXT', 'UA', 'UAA', 'UAL', 'UDR', 'UHS', 'ULTA', 'UNH', 'UNM', 'UNP', 'UPS', 'URI', 'USB', 'UTX', 'V', 'VAR', 'VFC', 'VIAB', 'VLO', 'VMC', 'VNO', 'VRSK', 'VRSN', 'VRTX', 'VTR', 'VZ', 'WAT', 'WBA', 'WDC', 'WEC', 'WELL', 'WFC', 'WHR', 'WLTW', 'WM', 'WMB', 'WMT', 'WRK', 'WU', 'WY', 'WYNN', 'XEC', 'XEL', 'XL', 'XLNX', 'XOM', 'XRAY', 'XRX', 'XYL', 'YUM', 'ZBH', 'ZION', 'ZTS']
    symbols.extend(['GPRO', 'IWM', 'SPY', 'ZNGA', 'IJR', 'IVV', 'VXF', 'ITA', 'VOT', 'VUG', 'VGT', 'IVW', 'VBK', 'IJH', 'IWF', 'IWO', 'IXN', 'IJT', 'IGM', 'IHI', 'VDE', 'VCR', 'VHT', 'VIS', 'VOO'])

    for sym in symbols:
        update_hdf_data(file, sym)

    print("Runtime: ", (datetime.datetime.now() - startTime))
